package xesoftware.ledgerposting.utils;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import xesoftware.ledgerposting.common.TimestampUtils;

import java.sql.Timestamp;

class TimestampUtilsTest {

    @Test
    void toTimestamp() {
        Assertions.assertEquals(Timestamp.valueOf("2024-10-10 00:00:00") , TimestampUtils.toTimestamp("2024-10-10"));
        Assertions.assertEquals(Timestamp.valueOf("2024-10-10 01:02:03") ,TimestampUtils.toTimestamp("2024-10-10 01:02:03"));
    }

    @Test
    void fromTimestamp() {
        Assertions.assertEquals("2024-10-10 00:00:00.0", TimestampUtils.fromTimestamp(Timestamp.valueOf("2024-10-10 00:00:00")));
    }
}