package xesoftware.ledgerposting.common.error;

public record ErrorResponse(int status, String error, String description, String path) {
}
