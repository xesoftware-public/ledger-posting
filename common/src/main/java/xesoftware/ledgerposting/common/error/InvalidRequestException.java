package xesoftware.ledgerposting.common.error;

import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public class InvalidRequestException extends RuntimeException {
    final HttpStatus httpStatus;
    public InvalidRequestException(final String description) {
        this(HttpStatus.BAD_REQUEST, description);
    }

    public InvalidRequestException(final HttpStatus httpStatus, final String description) {
        super(description);
        this.httpStatus = httpStatus;
    }
}
