package xesoftware.ledgerposting.common.error;

import lombok.NonNull;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;

@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(InvalidRequestException.class)
    public ResponseEntity<ErrorResponse> handleInvalidRequestException(@NonNull final InvalidRequestException ex, @NonNull final WebRequest request) {
        return buildErrorResponseEntity(ex.getHttpStatus(), ex.getMessage(), request);
    }

    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity<ErrorResponse> handleRuntimeException(@NonNull final RuntimeException ex, @NonNull final WebRequest request) {
       return buildErrorResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR, ex.getMessage(), request);
    }

    private static ResponseEntity<ErrorResponse> buildErrorResponseEntity(@NonNull final HttpStatus httpStatus, final String description, final WebRequest request) {
        return ResponseEntity.status(httpStatus).body(
                new ErrorResponse(httpStatus.value(), httpStatus.toString(), description,
                        (request instanceof ServletWebRequest)
                                ? ((ServletWebRequest)request).getRequest().getRequestURI()
                                : request.getContextPath())
        );
    }
}
