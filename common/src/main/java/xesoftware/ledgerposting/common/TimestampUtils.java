package xesoftware.ledgerposting.common;

import java.sql.Timestamp;

public class TimestampUtils {

    public static String TIMESTAMP_FORMAT_1 = "yyyy-mm-dd";
    public static String TIMESTAMP_FORMAT_2 = "yyyy-mm-dd hh:mm:ss";
    public static String TIMESTAMP_FORMAT_ERROR_MSG = "Timestamp must be in a valid format (" + TimestampUtils.TIMESTAMP_FORMAT_1 + " or " + TimestampUtils.TIMESTAMP_FORMAT_2 + ")";

    public static Timestamp toTimestamp(final String timestampStr) {
        try {
            return (null != timestampStr && !timestampStr.isEmpty()) ?
                    timestampStr.length() == 10
                            ? Timestamp.valueOf(timestampStr + " 00:00:00") // yyyy-mm-dd - 10 characters.
                            : Timestamp.valueOf(timestampStr)
                    : null;
        } catch (IllegalArgumentException ignore) {
            return null;
        }
    }

    public static String fromTimestamp(final Timestamp timestamp) {
        return (null != timestamp) ? timestamp.toString() : null;
    }
}
