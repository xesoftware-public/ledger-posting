## Java 21 microservice application with Springboot, Kafka, CQRS, JPA, Gradle, JUnit, Mockito, Cucumber, Swagger, Docker.


This ledger posting application consists of 3 domains...
- **account** - maintains account information.
- **journal** - maintains journal entries.
- **ledger** - journal entries are posted to the account ledgers.

Each domain is represented by a gradle sub-project and is further split into read (query) and write (command) service instances at runtime (for CQRS, by springboot profiles "query" and "command") and should allow the performance of the write operations to not be impacted by the read operations (and visa-versa). 

When a journal entry is made, it will automatically be posted to the ledger account, and the accounts ledger balances recalculated, based on the rules associated with the account type.

Communication between each service is via Kafka topic.

Each service has its own non-shared databases.

To simplify this application... 
- No account or journal entries can be modified. 
- All database persistence will be in-memory, 
- Kafka will also all be in-memory when running directly on locally machine.
- Springboots embedded Kafka broker will be exposed via a gradle task so there is no need to install Kafka.
- At the start-up of each service, each service will read its Kafka topic(s) from the start to get up-to-date.
- No security (api is all on http, and no authentication, authorisation)
- The same subproject contains the Query and the Command services, the instance of the springboot application will be for query or command depending on the active profile 'query' or 'command'
- Each service will be on its own port, from 8080 to 8084. (please ensure these are not in use)
- GUIDs are used for account and journal/ledger entities to ensure uniqueness without the overhead of centrally managing uniqueness of the entity ids.

## System Design

![](doc/ledger-posting-system-design.png)
- for account and journal, the entities are effectively persisted in the kafka topic.
  i.e. the incoming account or journal is put straight onto kafka.
- the read functionality, reads the kafka topic (from the beginning at service startup - ensuring always in sync) and puts the value in a database for the query.
- for ledger, we modify the balances, so they are stored in a database, before going into a kafka topic.

## Integration Tests.

- There is an integration test (`LedgerPostingControllersTest.java`) that runs through the happy path of creating accounts and posting journal entries to ledgers.

  see `$ ./gradlew :integration-tests:test`  (Uses random free ports, for the duration of the tests)
- Cucumber BDD integration tests.
  There are two feature tests, one to test account creation and one to test ledger posting.
  ![](doc/cucumber-tests.png)

## To run on a local machine.

### Note: nothing is persisted to file, everything is in-memory.

Run the entire system with gradle via command line or from intellij.
- Run embedded Kafka instance. (uses port 9092)
  - `$ ./gradlew :kafka:bootRun`
- Run rest of ledger-posting application. (uses ports 8080-8084)
  - `$ ./gradlew :account:bootRun-CQRS-Query`
  - `$ ./gradlew :account:bootRun-CQRS-Command`
  - `$ ./gradlew :journal:bootRun-CQRS-Query`
  - `$ ./gradlew :journal:bootRun-CQRS-Command`
  - `$ ./gradlew :ledger:bootRun-CQRS-Query`
  - `$ ./gradlew :ledger:bootRun-CQRS-Command`

Create some accounts with the account:bootRun-CQRS-Command api, (check results with account:bootRun-CQRS-Query api).
- See ... `/ledger-posting/account/src/test/http/test.http` ... for examples.
- You will need to note the GUID account ids for when creating journal entries

Create some journal entries with the journal:bootRun-CQRS-Command api, (check results with journal:bootRun-CQRS-Query api).
- See ...`/ledger-posting/journal/src/test/http/test.http` ... for examples.
- You may need to note the GUID journal ids for when viewing journal entries.
- creating a Journal entry will post the journal entry to the account's ledger and calculate the balances.
- You can see the ledger (posted journal) entries with ledger:bootRun-CQRS-Query api. 
- See ...`/ledger-posting/ledger/src/test/http/test.http` ... for examples on how to ...
  - Get a specific ledger entry based on the journal id.
  - Get all ledger entries for a specific account.
  - Get all ledger entries for a specific account, in a date range.
  - Get all ledger entries for a specific account, since a timestamp.
  - Get the ledger entry just before the timestamp.

- The timestamp format is `YYYY-MM-DD` or `YYYY-MM-DD HH:MM:SS` (no time assumes 00:00:00)
- Timezones are not supported.

## To run on docker.

- Run docker desktop.
- Run gradle build - _docker is not configured to build the application - it just deploys it_
- Run docker compose from the project directory.
   - `$ docker compose -f docker-compose.yml -p ledger-posting up -d`
- or run from docker-compose file in intellij.
- docker runs all services and a full version (not embedded) of kafka.
- kafka is persisted to `kafka/data` - so all data is kept between restarts.
- the same ports 8080 - 8084, and 9092 are used.
- All containers running looks like this in docker desktop.
![](doc/running-in-docker-compose.png)

## Swagger - Open API (auto generated)

When all the services are running, swagger api interactive documentation is available.

  - http://localhost:8080/swagger-ui/index.html for account:bootRun-CQRS-Query
  - http://localhost:8081/swagger-ui/index.html for account:bootRun-CQRS-Command
  - http://localhost:8082/swagger-ui/index.html for journal:bootRun-CQRS-Query
  - http://localhost:8083/swagger-ui/index.html for journal:bootRun-CQRS-Command
  - http://localhost:8084/swagger-ui/index.html for ledger:bootRun-CQRS-Query
  - There is no api for ledger:bootRun-CQRS-Command (so no swagger)

## References
- https://www.youtube.com/watch?v=8HWrCIFb4Q0 - Posting to a Ledger.
- https://www.youtube.com/watch?v=H18RWt-WUM4 - Journal Entries: The Basics and Analyzing Business Transactions.
- https://en.wikipedia.org/wiki/Debits_and_credits - Debits and Credits.
- https://courses.lumenlearning.com/suny-finaccounting/chapter/journalizing-and-posting-to-the-general-ledger - Posting to the General Ledger
- https://www.vinsguru.com/cqrs-pattern - CQRS Pattern.
- https://kafka.apache.org/intro - Kafka.
- https://docs.spring.io/spring-kafka/reference/testing.html - testing Kafka.
- https://medium.com/@dornala.amar/embedded-kafka-with-spring-boot-5cab7d8cf831 - Embedded Kafka with springboot.
- https://github.com/nanthakumaran-s/Learn-Kafka/blob/main/docker-setup.md - Kafka setup in docker compose.