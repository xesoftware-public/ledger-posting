package xesoftware.ledgerposting.ledger.controller;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import xesoftware.ledgerposting.common.TimestampUtils;
import xesoftware.ledgerposting.common.error.ErrorResponse;
import xesoftware.ledgerposting.ledger.dto.LedgerDto;
import xesoftware.ledgerposting.ledger.entity.Ledger;
import xesoftware.ledgerposting.ledger.repository.LedgerRepository;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(
        webEnvironment=SpringBootTest.WebEnvironment.RANDOM_PORT,
        properties = {"app.kafka.enabled=false" })
@ActiveProfiles("query")
class LedgerQueryControllerTest {

    @Autowired
    TestRestTemplate restTemplate;

    @MockBean
    private LedgerRepository ledgerRepository;

    @BeforeEach
    void setUp() {
    }

    @Test
    void getLedger() {
        Mockito.when(ledgerRepository.findById("YYY")).thenReturn(Optional.of(new Ledger("YYY", "ACID", Timestamp.from(Instant.now()),
                BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO)));
        ResponseEntity<LedgerDto> response = restTemplate.getForEntity("/ledger/qry/id/YYY", LedgerDto.class);

        assertNotNull(response);
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    void getBadLedger() {
        Mockito.when(ledgerRepository.findById("ZZZ")).thenReturn(Optional.empty());
        ResponseEntity<ErrorResponse> response = restTemplate.getForEntity("/ledger/qry/id/ZZZ", ErrorResponse.class);
        assertNotNull(response);
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals("Ledger with id 'ZZZ', was not found", response.getBody().description());
    }

    @Test
    void getPreviousLedger_BadTimestamp() {
    // /account/{accountId}/before/{before}
        ResponseEntity<ErrorResponse> response = restTemplate.getForEntity("/ledger/qry/account/ABC/before/999", ErrorResponse.class);
        assertNotNull(response);
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals("from timestamp value of '999', is invalid. Timestamp must be in a valid format (yyyy-mm-dd or yyyy-mm-dd hh:mm:ss)", response.getBody().description());
    }

    @Test
    void getPreviousLedger_GoodTimestamp() {
        Timestamp timestamp = Timestamp.from(Instant.now());
        Ledger ledger = new Ledger("YYY", "ABC", timestamp,
                BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO);
        Mockito.when(ledgerRepository.findPrevious("ABC", timestamp)).thenReturn(List.of(ledger));
        ResponseEntity<LedgerDto> response = restTemplate.getForEntity("/ledger/qry/account/ABC/before/" + timestamp, LedgerDto.class);
        assertNotNull(response);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(TimestampUtils.fromTimestamp(timestamp), response.getBody().getTimestamp());
        assertEquals("ABC", response.getBody().getAccountId());
    }

    @Test
    void getAllForAccount() {
        Mockito.when(ledgerRepository.findAllByAccountId("XXX")).thenReturn(Collections.emptyList());

        ResponseEntity<List<LedgerDto>> response = restTemplate.exchange("/ledger/qry/account/XXX", HttpMethod.GET, null, new ParameterizedTypeReference<List<LedgerDto>>() {});
        assertNotNull(response);
        assertEquals(HttpStatus.OK, response.getStatusCode());

        assertNotNull(response.getBody());
        assertTrue(response.getBody().isEmpty());

        List<Ledger> expectedLedgers = List.of
                (new Ledger("AAA", "ACID", Timestamp.from(Instant.now()),
                        BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO),
                        new Ledger("BBB", "ACID", Timestamp.from(Instant.now()),
                                BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO));
        Mockito.when(ledgerRepository.findAllByAccountId("ACID")).thenReturn(expectedLedgers);

        response = restTemplate.exchange("/ledger/qry/account/ACID", HttpMethod.GET, null, new ParameterizedTypeReference<List<LedgerDto>>() {});
        assertNotNull(response);
        assertEquals(HttpStatus.OK, response.getStatusCode());

        assertNotNull(response.getBody());
        assertEquals(2, response.getBody().size());
        assertEquals("ACID", response.getBody().getFirst().getAccountId());
        assertEquals("ACID", response.getBody().getLast().getAccountId());
    }

    @Test
    void getAllForAccount_BadTimestamps_1() {
        // /account/{accountId}/from/{from}/to/{to}
        ResponseEntity<ErrorResponse> response = restTemplate.getForEntity("/ledger/qry/account/ABC/from/111/to/222", ErrorResponse.class);
        assertNotNull(response);
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals("from timestamp value of '111', is invalid. Timestamp must be in a valid format (yyyy-mm-dd or yyyy-mm-dd hh:mm:ss)",
                response.getBody().description());
    }

    @Test
    void getAllForAccount_BadTimestamps_2() {
        // /account/{accountId}/from/{from}/to/{to}
        ResponseEntity<ErrorResponse> response = restTemplate.getForEntity("/ledger/qry/account/ABC/from/2020-01-01/to/222", ErrorResponse.class);
        assertNotNull(response);
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals("to timestamp value of '222', is invalid. Timestamp must be in a valid format (yyyy-mm-dd or yyyy-mm-dd hh:mm:ss)",
                response.getBody().description());
    }

    @Test
    void getAllForAccount_BadTimestamps_3() {
        // /account/{accountId}/from/{from}/to/{to}
        ResponseEntity<ErrorResponse> response = restTemplate.getForEntity("/ledger/qry/account/ABC/from/2024-01-01/to/2020-01-01", ErrorResponse.class);
        assertNotNull(response);
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals("Timestamp value of 'from' (2024-01-01) should be before timestamp value of 'to' (2020-01-01)",
                response.getBody().description());
    }
}