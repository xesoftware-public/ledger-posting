package xesoftware.ledgerposting.ledger.kafka;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import xesoftware.ledgerposting.ledger.dto.LedgerDto;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class LedgerEvent {
    private String type;
    private LedgerDto dto;
}
