package xesoftware.ledgerposting.ledger.kafka;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.common.TopicPartition;
import org.springframework.context.annotation.Profile;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.listener.AbstractConsumerSeekAware;
import org.springframework.stereotype.Service;
import xesoftware.ledgerposting.ledger.entity.Ledger;
import xesoftware.ledgerposting.ledger.repository.LedgerRepository;

import java.util.Map;

@Service
@Slf4j
@Profile("query")
public class LedgerKafkaListener extends AbstractConsumerSeekAware {

    private final LedgerRepository ledgerRepository;
    private final ObjectMapper objectMapper = new ObjectMapper();

    public LedgerKafkaListener(final LedgerRepository ledgerRepository) {

        this.ledgerRepository = ledgerRepository;
    }

    @Override
    public void onPartitionsAssigned(Map<TopicPartition, Long> assignments, ConsumerSeekCallback callback) {
        super.onPartitionsAssigned(assignments, callback);
        seekToBeginning(); // We always want to start at the beginning of the queue. as no persistent storage.
    }

    @KafkaListener(topics = KafkaInfo.TOPIC, groupId = KafkaInfo.GROUP)
    public void processEvent(@NonNull final String event) {

        log.info("Getting LedgerEvent {}", event);

        try {
            final LedgerEvent ledgerEvent = objectMapper.readValue(event, LedgerEvent.class);

            switch (ledgerEvent.getType()) {
                case KafkaInfo.CREATED_EVENT:
                case KafkaInfo.UPDATED_EVENT:
                    saveLedgerOnEvent(ledgerEvent);
                    break;
                default:
                    break;
            }

        } catch (final Exception e) {
            log.error("Failed to process JournalEvent", e);
        }
    }

    private void saveLedgerOnEvent(@NonNull final LedgerEvent ledgerEvent) {
        this.ledgerRepository.save(new Ledger(ledgerEvent.getDto()));
    }
}
