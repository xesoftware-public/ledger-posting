package xesoftware.ledgerposting.ledger.kafka;

public interface KafkaInfo {
    String TOPIC = "ledger";
    String GROUP = "ledger_group";
    String CREATED_EVENT = "created";
    String UPDATED_EVENT = "updated";
    String JOURNAL_TOPIC = "journal";
    String JOURNAL_LEDGER_GROUP = "journal_ledger_group";
}
