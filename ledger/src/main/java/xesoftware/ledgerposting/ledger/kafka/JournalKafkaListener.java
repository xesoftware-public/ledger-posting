package xesoftware.ledgerposting.ledger.kafka;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.common.TopicPartition;
import org.springframework.context.annotation.Profile;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.listener.AbstractConsumerSeekAware;
import org.springframework.stereotype.Service;
import xesoftware.ledgerposting.ledger.service.LedgerCommandService;

import java.util.Map;

// listens to journal and updates ledger with the journal entry.
@Service
@Slf4j
@Profile("command")
public class JournalKafkaListener extends AbstractConsumerSeekAware {

    private final LedgerCommandService ledgerCommandService;
    private final ObjectMapper objectMapper = new ObjectMapper();

    public JournalKafkaListener(final LedgerCommandService ledgerCommandService) {

        this.ledgerCommandService = ledgerCommandService;
    }

    @Override
    public void onPartitionsAssigned(Map<TopicPartition, Long> assignments, ConsumerSeekCallback callback) {
        super.onPartitionsAssigned(assignments, callback);
        seekToBeginning(); // We always want to start at the beginning of the queue. as no persistent storage.
    }

    @KafkaListener(topics = KafkaInfo.JOURNAL_TOPIC, groupId = KafkaInfo.JOURNAL_LEDGER_GROUP)
    public void processEvent(@NonNull final String event) {

        log.info("Getting JournalEvent {}", event);

        try {
            final JournalEvent journalEvent = objectMapper.readValue(event, JournalEvent.class);

            switch (journalEvent.getType()) {
                case KafkaInfo.CREATED_EVENT:
                    postJournalCreatedEvent(journalEvent);
                    break;
                default:
                    break;
            }

        } catch (final Exception e) {
            log.error("Failed to process JournalEvent", e);
        }
    }

    void postJournalCreatedEvent(@NonNull final JournalEvent journalEvent) {
        this.ledgerCommandService.postJournal(journalEvent.getDto(), journalEvent.getAccountName(), journalEvent.isDebitAccount());
    }
}
