package xesoftware.ledgerposting.ledger.kafka;

import lombok.Data;
import lombok.NoArgsConstructor;
import xesoftware.ledgerposting.ledger.dto.JournalDto;

@Data
@NoArgsConstructor
public class JournalEvent {
    private String type;
    private JournalDto dto;
    private String accountName;
    private boolean debitAccount;
}
