package xesoftware.ledgerposting.ledger.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import xesoftware.ledgerposting.ledger.dto.LedgerDto;

import java.math.BigDecimal;
import java.sql.Timestamp;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Ledger {
    @Id
    private String id; // same as JournalId
    private String accountId;
    private Timestamp tstamp;
    private BigDecimal debit;
    private BigDecimal credit;
    private BigDecimal debitBalance;
    private BigDecimal creditBalance;

    public Ledger(@NonNull final LedgerDto dto) {
        this.id = dto.getId();
        this.accountId = dto.getAccountId();
        this.tstamp = Timestamp.valueOf(dto.getTimestamp());
        this.debit = dto.getDebit();
        this.credit = dto.getCredit();
        this.debitBalance = dto.getDebitBalance();
        this.creditBalance = dto.getCreditBalance();
    }
}
