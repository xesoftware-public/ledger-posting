package xesoftware.ledgerposting.ledger.service;

import xesoftware.ledgerposting.ledger.dto.JournalDto;

public interface LedgerCommandService {
    void postJournal(JournalDto journal, String accountName, boolean debitAccount);
}
