package xesoftware.ledgerposting.ledger.service;

import lombok.NonNull;
import xesoftware.ledgerposting.ledger.dto.LedgerDto;

import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;

public interface LedgerQueryService {
    List<LedgerDto> getAll(@NonNull String accountId);
    List<LedgerDto> getAll(@NonNull String accountId, Timestamp fromTimestamp);
    List<LedgerDto> getAll(@NonNull String accountId, Timestamp fromTimestamp, Timestamp toTimestamp);
    Optional<LedgerDto> get(@NonNull String id);
    Optional<LedgerDto> getPrevious(@NonNull String accountId, Timestamp timestamp);
}
