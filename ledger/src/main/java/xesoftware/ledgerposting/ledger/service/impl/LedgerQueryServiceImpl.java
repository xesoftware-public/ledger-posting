package xesoftware.ledgerposting.ledger.service.impl;

import lombok.NonNull;
import org.springframework.stereotype.Service;
import xesoftware.ledgerposting.ledger.dto.LedgerDto;
import xesoftware.ledgerposting.ledger.repository.LedgerRepository;
import xesoftware.ledgerposting.ledger.service.LedgerQueryService;

import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;

@Service
public class LedgerQueryServiceImpl implements LedgerQueryService {
    final LedgerRepository ledgerRepository;

    public LedgerQueryServiceImpl(final LedgerRepository ledgerRepository) {
        this.ledgerRepository = ledgerRepository;
    }

    @Override
    public List<LedgerDto> getAll(@NonNull final String accountId, @NonNull final Timestamp fromTimestamp, @NonNull final Timestamp toTimestamp) {
        return this.ledgerRepository.findAllBetweenDates(accountId, fromTimestamp, toTimestamp).stream().map(LedgerDto::new).toList();
    }

    public List<LedgerDto> getAll(@NonNull final String accountId) {
        return this.ledgerRepository.findAllByAccountId(accountId).stream().map(LedgerDto::new).toList();
    }

    @Override
    public List<LedgerDto> getAll(@NonNull String accountId, @NonNull Timestamp fromTimestamp) {
        return this.ledgerRepository.findAllFrom(accountId, fromTimestamp).stream().map(LedgerDto::new).toList();
    }

    @Override
    public Optional<LedgerDto> get(@NonNull String id) {
        return this.ledgerRepository.findById(id).map(LedgerDto::new);
    }

    @Override
    public Optional<LedgerDto> getPrevious(@NonNull String accountId, @NonNull Timestamp timestamp) {
        return this.ledgerRepository.findPrevious(accountId, timestamp).stream().map(LedgerDto::new).findFirst();
    }
}
