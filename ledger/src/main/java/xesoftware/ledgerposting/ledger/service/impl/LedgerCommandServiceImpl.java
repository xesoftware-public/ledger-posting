package xesoftware.ledgerposting.ledger.service.impl;

import lombok.NonNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import xesoftware.ledgerposting.ledger.dto.JournalDto;
import xesoftware.ledgerposting.ledger.dto.LedgerDto;
import xesoftware.ledgerposting.ledger.entity.Ledger;
import xesoftware.ledgerposting.ledger.kafka.KafkaInfo;
import xesoftware.ledgerposting.ledger.kafka.LedgerEvent;
import xesoftware.ledgerposting.ledger.repository.LedgerRepository;
import xesoftware.ledgerposting.ledger.service.LedgerCommandService;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;

@Service
public class LedgerCommandServiceImpl implements LedgerCommandService {

    final LedgerRepository ledgerRepository;
    @Value("${app.kafka.enabled:false}")
    private boolean kafkaEnabled;
    private final KafkaTemplate<String, LedgerEvent> kafkaTemplate;

    public LedgerCommandServiceImpl(final LedgerRepository ledgerRepository, final KafkaTemplate<String, LedgerEvent> kafkaTemplate) {
        this.ledgerRepository = ledgerRepository;
        this.kafkaTemplate = kafkaTemplate;
    }

    @Override
    public void postJournal(@NonNull JournalDto journalDto, String accountName, boolean debitAccount) {
        final BigDecimal amount = new BigDecimal(journalDto.getAmount());
        // not sure but maybe need to alter debit/credit behaviour if amount is negative?
        final boolean isIncrease = amount.compareTo(BigDecimal.ZERO) >= 0;

        Ledger thisLedger = null;
        final Timestamp newLedgerTimestamp = Timestamp.valueOf(journalDto.getTimestamp());
        Timestamp fromLedgerTimestamp;

        final Optional<Ledger> existingLedgerOption = this.ledgerRepository.findById(journalDto.getId());
        if (existingLedgerOption.isPresent()) {
            thisLedger = existingLedgerOption.get();
            // existing ledger - remove it, update balances from existing ledgers date
            final Timestamp existingTimestamp = thisLedger.getTstamp();
            fromLedgerTimestamp = (existingTimestamp.compareTo(newLedgerTimestamp) <= 0)
                                    ? existingTimestamp // existingTimestamp is before or equal to newLedgerTimestamp.
                                    : newLedgerTimestamp; // newLedgerTimestamp is before existingTimestamp
        } else {
            // new ledger.
            thisLedger = new Ledger();
            thisLedger.setId(journalDto.getId());
            thisLedger.setAccountId(journalDto.getAccountId());
            thisLedger.setTstamp(newLedgerTimestamp);
            fromLedgerTimestamp = newLedgerTimestamp;
        }

        if (debitAccount) {
            // add to debit.
            thisLedger.setDebit(amount);
        } else {
            // add to credit.
            thisLedger.setCredit(amount);
        }

        // Save the ledger - without balances.
        this.ledgerRepository.save(thisLedger);
        if (kafkaEnabled) {
            kafkaTemplate.send(KafkaInfo.TOPIC, new LedgerEvent(KafkaInfo.CREATED_EVENT, new LedgerDto(thisLedger)));
        }

        // Get the balances from the ledger immediately prior to this timestamp.
        final AtomicReference<BigDecimal> creditBalanceRef = new AtomicReference<>(BigDecimal.ZERO);
        final AtomicReference<BigDecimal> debitBalanceRef = new AtomicReference<>(BigDecimal.ZERO);

        // get ledgers for account id with timestamp <= thisTimestamp, reverse order by timestamp, limit 1.
        final Optional<Ledger> previousOptional = this.ledgerRepository.findPrevious(journalDto.getAccountId(), fromLedgerTimestamp).stream().findFirst();

        previousOptional.ifPresent(previous -> {
            if (null != previous.getCreditBalance()) {
                creditBalanceRef.set(previous.getCreditBalance());
            }

            if (null != previous.getDebitBalance()) {
                debitBalanceRef.set(previous.getDebitBalance());
            }
        });

        // Recalculate all the balances from the timestamp to then end. and send a kafka event update for each ledger entry with an update.
        // get ledgers for account id with timestamp > thisTimestamp, order by timestamp.
        final List<Ledger> ledgersToUpdate = this.ledgerRepository.findAllFrom(journalDto.getAccountId(), fromLedgerTimestamp);

        ledgersToUpdate.forEach(ledger -> {
            if (debitAccount) {
                if (null != ledger.getDebit()) debitBalanceRef.set(debitBalanceRef.get().add(ledger.getDebit()));
                ledger.setDebitBalance(debitBalanceRef.get());
            } else {
                if (null != ledger.getCredit()) creditBalanceRef.set(creditBalanceRef.get().add(ledger.getCredit()));
                ledger.setCreditBalance(creditBalanceRef.get());
            }

            this.ledgerRepository.save(ledger);
            // send Kafka update.
            if (kafkaEnabled) {
                kafkaTemplate.send(KafkaInfo.TOPIC, new LedgerEvent(KafkaInfo.UPDATED_EVENT, new LedgerDto(ledger)));
            }
        });
    }
}
