package xesoftware.ledgerposting.ledger.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import xesoftware.ledgerposting.ledger.entity.Ledger;

import java.sql.Timestamp;
import java.util.List;

public interface LedgerRepository extends JpaRepository<Ledger, String> {
    @Query(value = "from Ledger l where l.accountId = :accountId AND l.tstamp BETWEEN :startTimestamp AND :endTimestamp")
    List<Ledger> findAllBetweenDates(@Param("accountId") String accountId,
                                     @Param("startTimestamp") Timestamp startTimestamp,
                                     @Param("endTimestamp") Timestamp endTimestamp);

    @Query(value = "from Ledger l where l.accountId = :accountId AND l.tstamp >= :fromTimestamp")
    List<Ledger> findAllFrom(@Param("accountId") String accountId,
                             @Param("fromTimestamp") Timestamp fromTimestamp);

    @Query(value = "from Ledger l where l.accountId = :accountId AND l.tstamp < :beforeTimestamp order by l.tstamp desc limit 1")
    List<Ledger> findPrevious(@Param("accountId") String accountId,
                              @Param("beforeTimestamp") Timestamp beforeTimestamp);

    List<Ledger> findAllByAccountId(@Param("accountId") String accountId);
}
