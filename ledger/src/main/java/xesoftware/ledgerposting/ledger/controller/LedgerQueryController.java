package xesoftware.ledgerposting.ledger.controller;

import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import xesoftware.ledgerposting.common.TimestampUtils;
import xesoftware.ledgerposting.common.error.InvalidRequestException;
import xesoftware.ledgerposting.ledger.dto.LedgerDto;
import xesoftware.ledgerposting.ledger.service.LedgerQueryService;

import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("ledger/qry")
@Profile("query")
public class LedgerQueryController {
    final LedgerQueryService ledgerQueryService;

    public LedgerQueryController(final LedgerQueryService ledgerQueryService) {
        this.ledgerQueryService = ledgerQueryService;
    }

    @GetMapping("/id/{id}")
    @ResponseStatus( HttpStatus.OK )
    public LedgerDto getLedger(@PathVariable(name = "id") final String id){
        return this.ledgerQueryService.get(id)
                .orElseThrow(() -> new InvalidRequestException(HttpStatus.NOT_FOUND, "Ledger with id '" + id + "', was not found"));
    }

    @GetMapping("/account/{accountId}")
    @ResponseStatus( HttpStatus.OK )
    public List<LedgerDto> getAllForAccount(@PathVariable(name = "accountId") final String accountId) {
        return this.ledgerQueryService.getAll(accountId);
    }

    @GetMapping("/account/{accountId}/before/{before}")
    @ResponseStatus( HttpStatus.OK )
    public Optional<LedgerDto> getPrevious(@PathVariable(name = "accountId") final String accountId,
                                            @PathVariable(name = "before") final String before) {
        final Timestamp beforeTimestamp = TimestampUtils.toTimestamp(before);
        if (null == beforeTimestamp) throw new InvalidRequestException("from timestamp value of '"+ before + "', is invalid. " + TimestampUtils.TIMESTAMP_FORMAT_ERROR_MSG);
        return this.ledgerQueryService.getPrevious(accountId, TimestampUtils.toTimestamp(before));
    }

    @GetMapping("/account/{accountId}/from/{from}")
    @ResponseStatus( HttpStatus.OK )
    public List<LedgerDto> getAllForAccount(@PathVariable(name = "accountId") final String accountId,
                                            @PathVariable(name = "from") final String from) {
        final Timestamp fromTimestamp = TimestampUtils.toTimestamp(from);
        if (null == fromTimestamp) throw new InvalidRequestException("from timestamp value of '"+ from + "', is invalid. " + TimestampUtils.TIMESTAMP_FORMAT_ERROR_MSG);
        return this.ledgerQueryService.getAll(accountId, fromTimestamp);
    }

    @GetMapping("/account/{accountId}/from/{from}/to/{to}")
    @ResponseStatus( HttpStatus.OK )
    public List<LedgerDto> getAllForAccount(@PathVariable(name = "accountId") final String accountId,
                                            @PathVariable(name = "from") final String from,
                                            @PathVariable(name = "to") final String to) {
        final Timestamp fromTimestamp = TimestampUtils.toTimestamp(from);
        if (null == fromTimestamp) throw new InvalidRequestException("from timestamp value of '"+ from + "', is invalid. " + TimestampUtils.TIMESTAMP_FORMAT_ERROR_MSG);
        final Timestamp toTimestamp = TimestampUtils.toTimestamp(to);
        if (null == toTimestamp) throw new InvalidRequestException("to timestamp value of '"+ to + "', is invalid. " + TimestampUtils.TIMESTAMP_FORMAT_ERROR_MSG);
        if (toTimestamp.before(fromTimestamp)) throw new InvalidRequestException("Timestamp value of 'from' (" + from + ") should be before timestamp value of 'to' (" + to + ")");
        return this.ledgerQueryService.getAll(accountId, fromTimestamp, toTimestamp);
    }

    @GetMapping("/ok")
    public String ok() {
        return "OK";
    }
}
