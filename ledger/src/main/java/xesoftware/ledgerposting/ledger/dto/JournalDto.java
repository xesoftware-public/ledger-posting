package xesoftware.ledgerposting.ledger.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class JournalDto {
    private String id;
    private String timestamp;
    private String amount;
    private String accountId;
}
