package xesoftware.ledgerposting.ledger.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import xesoftware.ledgerposting.ledger.entity.Ledger;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
public class LedgerDto {
    private String id; // same as JournalId
    private String accountId;
    private String timestamp;
    private BigDecimal debit;
    private BigDecimal credit;
    private BigDecimal debitBalance;
    private BigDecimal creditBalance;

    public LedgerDto(@NonNull final Ledger ledger) {
        this.id = ledger.getId();
        this.accountId = ledger.getAccountId();
        this.timestamp = ledger.getTstamp().toString();
        this.debit = ledger.getDebit();
        this.credit = ledger.getCredit();
        this.debitBalance = ledger.getDebitBalance();
        this.creditBalance = ledger.getCreditBalance();
    }
}
