Feature: Maintain multiple accounts

  Scenario: Create and get multiple accounts
    Given All account services are running
    Given I want to define some accounts
      | name  | description   | debit |
      | one   | one account   | true  |
      | two   | two account   | false |
      | three | three account | true  |
      | four  | four account  | false |
    When I post the accounts
    Then I eventually get each account back
    Then I get all the accounts back
    Then I get the account with the name "one"
    Then I get the account with the name "two"
    Then I get the account with the name "three"
    Then I get the account with the name "four"
