Feature: Full ledger posting

  Scenario: Create accounts and journal entries and check ledger entries created
    Given All services required for testing journal are running
    Given I want to create some accounts
      | name  | description   | debit |
      | one   | one account   | true  |
      | two   | two account   | false |
    When I create some journal entries
      | name  | timestamp             | amount  |
      | one   | 2024-01-01 08:00:00.0 | 124.00  |
      | two   | 2024-01-01 08:00:01.0 | 456.00  |
      | one   | 2024-01-01 08:00:02.0 | 100.00  |
      | two   | 2024-01-01 08:00:03.0 | 200.00  |
    Then I eventually get each journal entry back
    Then I eventually get each ledger entry back
    Then I get the following ledger entries for account named 'one'
      | timestamp             | debit       | debitBalance | credit     | creditBalance |
      | 2024-01-01 08:00:00.0 | 124.00      | 124.00       |            |               |
      | 2024-01-01 08:00:02.0 | 100.00      | 224.00       |            |               |
    Then I get the following ledger entries for account named 'two'
      | timestamp             | debit       | debitBalance | credit     | creditBalance |
      | 2024-01-01 08:00:01.0 |             |              | 456.00     | 456.00        |
      | 2024-01-01 08:00:03.0 |             |              | 200.00     | 656.00        |
