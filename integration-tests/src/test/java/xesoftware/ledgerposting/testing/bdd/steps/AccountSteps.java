package xesoftware.ledgerposting.testing.bdd.steps;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.springframework.http.HttpStatus;
import xesoftware.ledgerposting.testing.bdd.CucumberIntegrationTestBase;

public class AccountSteps extends CucumberIntegrationTestBase {

    final AccountHelper accountHelper = new AccountHelper();

    @Given("All account services are running")
    public void allServicesAreRunning() {
        startAllAccountApiServices(embeddedKafka);
        checkAllAccountApiServicesRunningOK();
    }

    @Given("I want to define some accounts")
    public void iWantToCreateAnAccount(final DataTable table) {
        this.accountHelper.createAccounts(table);
    }

    @When("I post the accounts")
    public void iPostTheAccount() {
        this.accountHelper.checkPostAccounts(restTemplateAccountCmd);
    }

    @Then("I eventually get each account back")
    public void iGetEachAccountBack() {
        this.accountHelper.checkEachAccountReturned(restTemplateAccountQry);
    }

    @Then("I get the account with the name {string}")
    public void iGetTheNamedAccount(final String name) {
        accountHelper.checkGetAccountByName(restTemplateAccountQry, name, HttpStatus.OK);
    }

    @Then("I get all the accounts back")
    public void iEventuallyGetTheAccountBack() {
        accountHelper.checkAllAccountsReturned(restTemplateAccountQry);
    }
}
