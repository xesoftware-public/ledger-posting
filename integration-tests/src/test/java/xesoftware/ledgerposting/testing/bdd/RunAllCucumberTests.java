package xesoftware.ledgerposting.testing.bdd;


import org.junit.platform.suite.api.IncludeEngines;
import org.junit.platform.suite.api.SelectClasspathResource;
import org.junit.platform.suite.api.Suite;

@Suite
@IncludeEngines("cucumber")
@SelectClasspathResource("features")
public class RunAllCucumberTests {
    // https://github.com/cronn/cucumber-junit5-example/tree/main
}