package xesoftware.ledgerposting.testing.bdd.steps;

import io.cucumber.datatable.DataTable;
import lombok.Getter;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import xesoftware.ledgerposting.account.dto.AccountDto;
import xesoftware.ledgerposting.account.dto.CreateAccountDto;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static xesoftware.ledgerposting.testing.TestingUtils.tryNTimes;
import static org.assertj.core.api.Assertions.*;
import static xesoftware.ledgerposting.testing.IntegrationTestBase.*;

@Getter
public class AccountHelper {
    List<CreateAccountDto> createAccountDtoList;
    List<AccountDto> accountDtoList;
    AccountDto accountDto;
    Map<String, String> nameToAccountIdMap;

    public void createAccounts(final DataTable table) {
        this.createAccountDtoList = table.asMaps(String.class, String.class).stream()
                .map(row -> new CreateAccountDto(
                        row.get("name"), row.get("description"), Boolean.parseBoolean(row.get("debit"))
                )).toList();
    }

    public void checkPostAccounts(final TestRestTemplate testRestTemplate) {
        accountDtoList = new ArrayList<>();
        nameToAccountIdMap = new HashMap<>();
        createAccountDtoList.forEach(dto -> {
            var response = createAccount(testRestTemplate,dto);
            checkAccountResponseEntity(response, HttpStatus.CREATED, dto);

            assertNotNull(response);
            assertEquals(response.getStatusCode(), HttpStatus.CREATED);
            assertNotNull(response.getBody());
            accountDtoList.add(response.getBody());
            nameToAccountIdMap.put(response.getBody().getName(), response.getBody().getId());
        });
    }

    public void checkEachAccountReturned(final TestRestTemplate testRestTemplate) {
        accountDtoList.forEach(dto -> {
            var response = tryNTimes(() -> getAccount(testRestTemplate, dto.getId()), r -> HttpStatus.OK == r.getStatusCode());
            assertNotNull(response);
            assertEquals(HttpStatus.OK, response.getStatusCode());
            assertNotNull(response.getBody());
            assertEquals(dto.getId(), response.getBody().getId());
            assertEquals(dto.getName(), response.getBody().getName());
            assertEquals(dto.getDescription(), response.getBody().getDescription());
            assertEquals(dto.isDebitAccount(), response.getBody().isDebitAccount());
        });
    }

    public void checkGetAccountByName(final TestRestTemplate testRestTemplate, final String name, final HttpStatus expectedStatus) {
        var response = tryNTimes(() -> getAccountByName(testRestTemplate, name), r -> HttpStatus.OK == r.getStatusCode());
        assertNotNull(response);
        assertEquals(expectedStatus, response.getStatusCode());
        assertNotNull(response.getBody());
        if (expectedStatus.is2xxSuccessful()) {
            this.accountDto = response.getBody();
            assertEquals(name, this.accountDto.getName());
        }
    }

    public void checkAllAccountsReturned(final TestRestTemplate testRestTemplate) {
        var response = getAllAccounts(testRestTemplate);
        assertNotNull(response);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertThat(accountDtoList).hasSameElementsAs(response.getBody());
    }
}
