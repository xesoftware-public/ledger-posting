package xesoftware.ledgerposting.testing.bdd;

import io.cucumber.spring.CucumberContextConfiguration;
import xesoftware.ledgerposting.testing.IntegrationTestBase;

/**
 * Cucumber integration test base class. Derive the step definition classes from this.
 * Because only one class can configure the spring/cucumber context, we must have this common base for cucumber tests.
 * Also, it cannot be an abstract class, even though IntegrationTestBase is abstract!
 */
@CucumberContextConfiguration
public class CucumberIntegrationTestBase extends IntegrationTestBase {
}
