package xesoftware.ledgerposting.testing.bdd.steps;

import io.cucumber.datatable.DataTable;
import lombok.NonNull;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import xesoftware.ledgerposting.journal.dto.JournalDto;
import xesoftware.ledgerposting.ledger.dto.LedgerDto;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static xesoftware.ledgerposting.testing.TestingUtils.tryNTimes;
import static xesoftware.ledgerposting.testing.IntegrationTestBase.*;

import java.math.BigDecimal;
import java.util.List;

public class LedgerHelper {
    final AccountHelper accountHelper;

    public LedgerHelper(@NonNull final AccountHelper accountHelper) {
        this.accountHelper = accountHelper;
    }

    void checkLedgersForAccount(final TestRestTemplate testRestTemplate, final DataTable table, final String accountName) {
        // get all for account.
        final String accountId = this.accountHelper.getNameToAccountIdMap().get(accountName);
        final List<LedgerDto> ledgerDtoList = getAllLedgersForAccount(testRestTemplate, accountId);
        final var tableMap = table.asMaps(String.class, String.class);

        assertEquals(tableMap.size(), ledgerDtoList.size(), "Unexpected number of ledgers in table");
        table.asMaps(String.class, String.class).forEach(row -> {
            var timestamp = row.get("timestamp");
            var debit = row.get("debit");
            var credit = row.get("credit");
            var debitBalance = row.get("debitBalance");
            var creditBalance = row.get("creditBalance");
            var found = ledgerDtoList.stream().filter(dto -> dto.getTimestamp().equals(timestamp)).findFirst();
            assertTrue(found.isPresent(), "Did not find ledger with timestamp " + timestamp);
            found.ifPresent(foundDto -> {
                assertEquals(accountId, foundDto.getAccountId());
                assertEquals((null != debit)         ? new BigDecimal(debit)         : null, foundDto.getDebit());
                assertEquals((null != debitBalance)  ? new BigDecimal(debitBalance)  : null, foundDto.getDebitBalance());
                assertEquals((null != credit)        ? new BigDecimal(credit)        : null, foundDto.getCredit());
                assertEquals((null != creditBalance) ? new BigDecimal(creditBalance) : null, foundDto.getCreditBalance());
            });
        });
    }

    public void checkEachLedgerEntryReturned(final TestRestTemplate testRestTemplate, final List<JournalDto> journalDtoList) {
        journalDtoList.forEach(dto -> {
            var response = tryNTimes(() -> getLedger(testRestTemplate, dto.getId()), r -> HttpStatus.OK == r.getStatusCode());
            assertNotNull(response);
            assertEquals(HttpStatus.OK, response.getStatusCode());
            assertNotNull(response.getBody());
            assertEquals(dto.getId(), response.getBody().getId());
            assertEquals(dto.getAccountId(), response.getBody().getAccountId());
            assertEquals(dto.getTimestamp(), response.getBody().getTimestamp());
        });
    }
}
