package xesoftware.ledgerposting.testing.bdd.steps;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import xesoftware.ledgerposting.testing.bdd.CucumberIntegrationTestBase;

public class LedgerPostingSteps extends CucumberIntegrationTestBase {
    private final AccountHelper accountHelper = new AccountHelper();
    private final JournalHelper journalHelper = new JournalHelper(accountHelper);
    private final LedgerHelper ledgerHelper = new LedgerHelper(accountHelper);

    @Given("All services required for testing journal are running")
    public void allJournalServicesAreRunning() {
        startAllAccountApiServices(embeddedKafka);
        checkAllAccountApiServicesRunningOK();

        startAllJournalApiServices(embeddedKafka);
        checkAllJournalApiServicesRunningOK();

        startAllLedgerApiServices(embeddedKafka);
        checkAllLedgerApiServicesRunningOK();
    }

    @Given("I want to create some accounts")
    public void iWantToCreateAnAccount(final DataTable table) {
        this.accountHelper.createAccounts(table);
        this.accountHelper.checkPostAccounts(restTemplateAccountCmd);
        this.accountHelper.checkEachAccountReturned(restTemplateAccountQry);
    }

    @When("I create some journal entries")
    public void iCreateSomeJournalEntries(final DataTable table) {
        this.journalHelper.createJournals(table);
        this.journalHelper.checkPostJournals(restTemplateJournalCmd);
    }

    @Then("I eventually get each journal entry back")
    public void iEventuallyGetEachJournalEntryBack() {
        this.journalHelper.checkEachJournalEntryReturned(restTemplateJournalQry);
    }

    @Then("I eventually get each ledger entry back")
    public void iEventuallyGetEachLedgerEntryBack() {
        this.ledgerHelper.checkEachLedgerEntryReturned(restTemplateLedgerQry, journalHelper.getJournalDtoList());
    }

    @Then("I get the following ledger entries for account named {string}")
    public void iGetTheFollowingLedgerEntriesForAccountNamedOne(final String accountName, final DataTable table) {
        this.ledgerHelper.checkLedgersForAccount(restTemplateLedgerQry, table, accountName);
    }
}
