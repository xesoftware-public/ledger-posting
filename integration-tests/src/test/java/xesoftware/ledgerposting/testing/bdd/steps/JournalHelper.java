package xesoftware.ledgerposting.testing.bdd.steps;

import io.cucumber.datatable.DataTable;
import lombok.Getter;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import xesoftware.ledgerposting.journal.dto.CreateJournalDto;
import xesoftware.ledgerposting.journal.dto.JournalDto;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static xesoftware.ledgerposting.testing.TestingUtils.tryNTimes;
import static xesoftware.ledgerposting.testing.IntegrationTestBase.*;

@Getter
public class JournalHelper {
    private final AccountHelper accountHelper;

    private List<CreateJournalDto> createJournalDtoList;
    private List<JournalDto> journalDtoList;

    public JournalHelper(final AccountHelper accountHelper) {
        this.accountHelper = accountHelper;
    }

    void createJournals(final DataTable table) {
        this.createJournalDtoList = table.asMaps(String.class, String.class).stream()
            .map(row -> new CreateJournalDto(
                    row.get("timestamp"),
                    row.get("amount"),
                    this.accountHelper.getNameToAccountIdMap().get(row.get("name")))
            ).toList();
    }

    public void checkPostJournals(final TestRestTemplate testRestTemplate) {
        journalDtoList = new ArrayList<>();
        createJournalDtoList.forEach(dto -> {
            // lookup accountId from account name.
            var response = createJournalOk(testRestTemplate,dto);
            journalDtoList.add(response);
        });
    }

    public void checkEachJournalEntryReturned(final TestRestTemplate testRestTemplate) {
        journalDtoList.forEach(dto -> {
            var response = tryNTimes(() -> getJournal(testRestTemplate, dto.getId()), r -> HttpStatus.OK == r.getStatusCode());
            assertNotNull(response);
            assertEquals(HttpStatus.OK, response.getStatusCode());
            assertNotNull(response.getBody());
            assertEquals(dto.getId(), response.getBody().getId());
            assertEquals(dto.getAccountId(), response.getBody().getAccountId());
            assertEquals(dto.getTimestamp(), response.getBody().getTimestamp());
            assertEquals(dto.getAmount(), response.getBody().getAmount());
        });
    }
}
