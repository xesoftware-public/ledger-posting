package xesoftware.ledgerposting.testing;

import java.util.function.Function;
import java.util.function.Supplier;

public class TestingUtils {

    public static <T> T tryNTimes(final Supplier<T> supplier, final Function<T, Boolean> isOk) {
        return tryNTimes(supplier, 1000, 10, isOk);
    }

    public static <T> T tryNTimes(final Supplier<T> supplier, final int delayMs, final int maxTimes, final Function<T, Boolean> isOk) {
        int count = 0;
        T ret;
        boolean tryAgain = false;
        boolean ok;
        do {
            if (tryAgain) {
                try {
                    Thread.sleep(delayMs);
                } catch (InterruptedException ignore) {
                    System.err.println("Interrupted while waiting for " + delayMs + " ms");
                    return null;
                }
            }
            ret = supplier.get();
            ok = isOk.apply(ret);
            count++;
            tryAgain = count < maxTimes && !ok;

        } while (tryAgain);

        if (count > 1) {
            System.out.println("Tried " + count + " times (taking " + ((count * delayMs) / 1000) + " seconds) before ultimately " + ((ok) ? "succeeding." : "failing.") );
        }

        return ret;
    }
}
