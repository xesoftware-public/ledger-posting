package xesoftware.ledgerposting.testing;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.*;
import org.springframework.http.HttpStatus;
import xesoftware.ledgerposting.account.dto.AccountDto;
import xesoftware.ledgerposting.account.dto.CreateAccountDto;
import xesoftware.ledgerposting.journal.dto.CreateJournalDto;
import xesoftware.ledgerposting.journal.dto.JournalDto;

import java.math.BigDecimal;
import java.util.List;

import static xesoftware.ledgerposting.testing.TestingUtils.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class LedgerPostingAPITest extends IntegrationTestBase {

    @Test
    @Order(1)
    public void checkAllApiServicesRunningOK() {
        super.checkAllApiServicesRunningOK();
    }

    @Test
    @Order(2)
    public void runHappyPathTests() throws InterruptedException {

        //////////////////////// CREATE 1ST ACCOUNT

        final var createAccountDto_1 = new CreateAccountDto("Debit-1", "Debit account", true);

        var responseEntity = createAccount(restTemplateAccountCmd, createAccountDto_1);
        assertNotNull(responseEntity);
        assertNotNull(responseEntity.getBody());
        assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
        final String accountId_1 = responseEntity.getBody().getId();

        // should not be able to create an account on the qry.
        responseEntity = createAccount(restTemplateAccountQry, createAccountDto_1);
        assertNotNull(responseEntity);
        assertEquals(HttpStatus.NOT_FOUND, responseEntity.getStatusCode());

        responseEntity = tryNTimes(() -> getAccount(restTemplateAccountQry, accountId_1), r -> HttpStatus.OK == r.getStatusCode());

        var entity = checkAccountResponseEntity(responseEntity, HttpStatus.OK, createAccountDto_1);
        assertEquals(accountId_1, entity.getId());

        //////////////////////// CREATE 2ND ACCOUNT

        final var createAccountDto_2 = new CreateAccountDto("Credit-1", "Credit account", false);

        responseEntity = createAccount(restTemplateAccountCmd, createAccountDto_2);
        assertNotNull(responseEntity);
        assertNotNull(responseEntity.getBody());
        assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
        final String accountId_2 = responseEntity.getBody().getId();

        responseEntity = tryNTimes(() -> getAccount(restTemplateAccountQry, accountId_2), r -> HttpStatus.OK == r.getStatusCode());

        entity = checkAccountResponseEntity(responseEntity, HttpStatus.OK, createAccountDto_2);
        assertEquals(accountId_2, entity.getId());

        //////////////////////// LIST BOTH ACCOUNTS
        var allAccountsEntity = getAllAccounts(restTemplateAccountQry);
        assertNotNull(allAccountsEntity);
        assertEquals(HttpStatus.OK, allAccountsEntity.getStatusCode());
        assertNotNull(allAccountsEntity.getBody());

        final List<AccountDto> allAccounts = allAccountsEntity.getBody();
        assertEquals(accountId_1, allAccounts.getFirst().getId());
        assertEquals(accountId_2, allAccounts.getLast().getId());

        //////////////////////// ADD JOURNAL ENTRIES

        var journalDto_1 = createJournalOk(restTemplateJournalCmd, new CreateJournalDto("2024-01-01 08:00:00.0","123.00", accountId_1));
        var journalDto_2 = createJournalOk(restTemplateJournalCmd, new CreateJournalDto("2024-01-01 09:00:00.0","456.00", accountId_1));
        var journalDto_3 = createJournalOk(restTemplateJournalCmd, new CreateJournalDto("2024-01-01 10:00:00.0","789.00", accountId_2));
        var journalDto_4 = createJournalOk(restTemplateJournalCmd, new CreateJournalDto("2024-01-01 11:00:00.0","123.00", accountId_2));

        testGetJournal(restTemplateJournalQry, journalDto_1);
        testGetJournal(restTemplateJournalQry, journalDto_2);
        testGetJournal(restTemplateJournalQry, journalDto_3);
        testGetJournal(restTemplateJournalQry, journalDto_4);

        var allJournalsEntity = getAllJournals(restTemplateJournalQry);
        assertNotNull(allJournalsEntity);
        assertEquals(HttpStatus.OK, allJournalsEntity.getStatusCode());
        assertNotNull(allJournalsEntity.getBody());

        final List<JournalDto> allJournals = allJournalsEntity.getBody();
        assertJournal(journalDto_1, allJournals.get(0));
        assertJournal(journalDto_2, allJournals.get(1));
        assertJournal(journalDto_3, allJournals.get(2));
        assertJournal(journalDto_4, allJournals.get(3));

        //////////////////////// GET LEDGER ENTRIES

        testGetLedger(restTemplateLedgerQry, journalDto_1);
        testGetLedger(restTemplateLedgerQry, journalDto_2);
        testGetLedger(restTemplateLedgerQry, journalDto_3);
        testGetLedger(restTemplateLedgerQry, journalDto_4);

        var accountLedgers_1 = getAllLedgersForAccount(restTemplateLedgerQry, accountId_1);
        assertEquals(2, accountLedgers_1.size());

        var accountLedgers_2 = getAllLedgersForAccount(restTemplateLedgerQry, accountId_2);
        assertEquals(2, accountLedgers_2.size());

        assertLedger(journalDto_1, accountLedgers_1.get(0));
        assertLedgerAmounts(accountLedgers_1.get(0), null, null,new BigDecimal("123.00"),new BigDecimal("123.00"));
        assertLedger(journalDto_2, accountLedgers_1.get(1));
        assertLedgerAmounts(accountLedgers_1.get(1), null, null,new BigDecimal("456.00"),new BigDecimal("579.00"));
        assertLedger(journalDto_3, accountLedgers_2.get(0));
        assertLedgerAmounts(accountLedgers_2.get(0), new BigDecimal("789.00"),new BigDecimal("789.00"), null, null);
        assertLedger(journalDto_4, accountLedgers_2.get(1));
        assertLedgerAmounts(accountLedgers_2.get(1), new BigDecimal("123.00"),new BigDecimal("912.00"), null, null);
    }
}
