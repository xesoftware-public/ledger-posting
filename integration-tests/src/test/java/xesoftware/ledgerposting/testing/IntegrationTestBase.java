package xesoftware.ledgerposting.testing;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.test.EmbeddedKafkaBroker;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.stereotype.Component;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.util.TestSocketUtils;
import xesoftware.ledgerposting.account.AccountApplication;
import xesoftware.ledgerposting.account.dto.AccountDto;
import xesoftware.ledgerposting.account.dto.CreateAccountDto;
import xesoftware.ledgerposting.journal.JournalApplication;
import xesoftware.ledgerposting.journal.dto.CreateJournalDto;
import xesoftware.ledgerposting.journal.dto.JournalDto;
import xesoftware.ledgerposting.ledger.LedgerApplication;
import xesoftware.ledgerposting.ledger.dto.LedgerDto;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import static xesoftware.ledgerposting.testing.TestingUtils.*;

@Component
@DirtiesContext
@EmbeddedKafka(partitions = 1, topics = {"account","journal","ledger"})
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
abstract public class IntegrationTestBase {

    private final static String rootUrl = "http://localhost";

    private final int accountCmdApiPort;
    private final int accountQryApiPort;
    private final int journalCmdApiPort;
    private final int journalQryApiPort;
    private final int ledgerQryApiPort;

    private static TestRestTemplate createRestTemplate(int port) { return new TestRestTemplate(new RestTemplateBuilder().rootUri(rootUrl+":"+port)); }

    protected final TestRestTemplate restTemplateAccountCmd;
    protected final TestRestTemplate restTemplateAccountQry;
    protected final TestRestTemplate restTemplateJournalCmd;
    protected final TestRestTemplate restTemplateJournalQry;
    protected final TestRestTemplate restTemplateLedgerQry;

    @Autowired
    protected EmbeddedKafkaBroker embeddedKafka; // used by derived Step definitions.

    public IntegrationTestBase() {
        this.accountCmdApiPort = TestSocketUtils.findAvailableTcpPort();
        this.accountQryApiPort = TestSocketUtils.findAvailableTcpPort();
        this.journalCmdApiPort = TestSocketUtils.findAvailableTcpPort();
        this.journalQryApiPort = TestSocketUtils.findAvailableTcpPort();
        this.ledgerQryApiPort = TestSocketUtils.findAvailableTcpPort();

        this.restTemplateAccountCmd = createRestTemplate(accountCmdApiPort);
        this.restTemplateAccountQry = createRestTemplate(accountQryApiPort);
        this.restTemplateJournalCmd = createRestTemplate(journalCmdApiPort);
        this.restTemplateJournalQry = createRestTemplate(journalQryApiPort);
        this.restTemplateLedgerQry = createRestTemplate(ledgerQryApiPort);
    }

    @BeforeAll
    public void startAllApiServices(final EmbeddedKafkaBroker embeddedKafka) {
        // @Autowired this.embeddedKafka will be null. Use injected parameter instead.
        startAllAccountApiServices(embeddedKafka);
        startAllLedgerApiServices(embeddedKafka);
        startAllJournalApiServices(embeddedKafka);
    }

    public void startAllAccountApiServices(final EmbeddedKafkaBroker embeddedKafka) {
        System.out.println("Starting account services ... ");
        new SpringApplicationBuilder(AccountApplication.class)
                .profiles("command")
                .properties("server.port=" + accountCmdApiPort,
                        "spring.kafka.bootstrap-servers=" + embeddedKafka.getBrokersAsString()
                        /*, "logging.level.org.springframework=TRACE" */)
                .run();

        new SpringApplicationBuilder(AccountApplication.class)
                .profiles("query")
                .properties("server.port=" + accountQryApiPort,
                        "spring.kafka.bootstrap-servers=" + embeddedKafka.getBrokersAsString()
                        /*, "logging.level.org.springframework=TRACE" */)
                .run();
    }

    public void startAllJournalApiServices(final EmbeddedKafkaBroker embeddedKafka) {
        System.out.println("Starting ledger services ... ");

        new SpringApplicationBuilder(JournalApplication.class)
                .profiles("command")
                .properties("server.port="+journalCmdApiPort,
                        "spring.kafka.bootstrap-servers="+embeddedKafka.getBrokersAsString()
                        /*, "logging.level.org.springframework=TRACE" */)
                .run();

        new SpringApplicationBuilder(JournalApplication.class)
                .profiles("query")
                .properties("server.port="+journalQryApiPort,
                        "spring.kafka.bootstrap-servers="+embeddedKafka.getBrokersAsString()
                        /*, "logging.level.org.springframework=TRACE" */)
                .run();
    }

    public void startAllLedgerApiServices(final EmbeddedKafkaBroker embeddedKafka) {
        System.out.println("Starting journal services ... ");
        new SpringApplicationBuilder(LedgerApplication.class)
                .profiles("command")
                .web(WebApplicationType.NONE)
                .properties(
                        "spring.kafka.bootstrap-servers="+embeddedKafka.getBrokersAsString()
                        /*, "logging.level.org.springframework=TRACE" */)
                .run();

        new SpringApplicationBuilder(LedgerApplication.class)
                .profiles("query")
                .properties("server.port="+ledgerQryApiPort,
                        "spring.kafka.bootstrap-servers="+embeddedKafka.getBrokersAsString()
                        /*, "logging.level.org.springframework=TRACE" */)
                .run();
    }

    public void checkAllApiServicesRunningOK() {
        checkAllAccountApiServicesRunningOK();
        checkAllJournalApiServicesRunningOK();
        checkAllLedgerApiServicesRunningOK();
    }

    public void checkAllAccountApiServicesRunningOK() {
        assertEquals("OK", getString("/account/cmd/ok", restTemplateAccountCmd).getBody());
        assertEquals("OK", getString("/account/qry/ok", restTemplateAccountQry).getBody());
    }

    public void checkAllJournalApiServicesRunningOK() {
        assertEquals("OK", getString("/journal/cmd/ok", restTemplateJournalCmd).getBody());
        assertEquals("OK", getString("/journal/qry/ok", restTemplateJournalQry).getBody());
    }

    public void checkAllLedgerApiServicesRunningOK() {
        assertEquals("OK", getString("/ledger/qry/ok", restTemplateLedgerQry).getBody());
    }

    public static AccountDto checkAccountResponseEntity(final ResponseEntity<AccountDto> responseEntity,
                                                        final HttpStatus httpStatus,
                                                        final CreateAccountDto createAccountDto) {
        assertNotNull(responseEntity);
        AccountDto accountDto = responseEntity.getBody();
        assertEquals(httpStatus, responseEntity.getStatusCode());
        assertNotNull(accountDto);

        assertEquals(createAccountDto.isDebitAccount(), accountDto.isDebitAccount());
        assertEquals(createAccountDto.getName(), accountDto.getName());
        assertEquals(createAccountDto.getDescription(), accountDto.getDescription());

        return accountDto;
    }

    static void testGetJournal(final TestRestTemplate restTemplate, final JournalDto dto) {
        final var responseEntity = tryNTimes(() -> getJournal(restTemplate, dto.getId()), r -> HttpStatus.OK == r.getStatusCode());
        assertNotNull(responseEntity);
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertNotNull(responseEntity.getBody());
        assertJournal(dto, responseEntity.getBody());
    }

    static void assertJournal(final JournalDto expected, final JournalDto actual) {
        assertEquals(expected.getId(), actual.getId());
        assertEquals(expected.getAccountId(), actual.getAccountId());
        assertEquals(expected.getAmount(), actual.getAmount());
        assertEquals(expected.getTimestamp(), actual.getTimestamp());
    }

    static void testGetLedger(final TestRestTemplate restTemplate, final JournalDto dto) {
        final var responseEntity = tryNTimes(() -> getLedger(restTemplate, dto.getId()), r -> HttpStatus.OK == r.getStatusCode());
        assertNotNull(responseEntity);
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertNotNull(responseEntity.getBody());
        assertLedger(dto, responseEntity.getBody());
    }

    static void assertLedger(final JournalDto expected, final LedgerDto actual) {
        assertEquals(expected.getId(), actual.getId());
        assertEquals(expected.getAccountId(), actual.getAccountId());
        assertEquals(expected.getTimestamp(), actual.getTimestamp());
    }

    static void assertLedgerAmounts(final LedgerDto actual, final BigDecimal credit, final BigDecimal creditBalance, final BigDecimal debit, final BigDecimal debitBalance) {
        assertEquals(credit, actual.getCredit());
        assertEquals(creditBalance, actual.getCreditBalance());
        assertEquals(debit, actual.getDebit());
        assertEquals(debitBalance, actual.getDebitBalance());
    }

    public static ResponseEntity<AccountDto> createAccount(final TestRestTemplate restTemplate, final CreateAccountDto dto) {
        return restTemplate.postForEntity("/account/cmd/create", dto, AccountDto.class);
    }

    public static ResponseEntity<AccountDto> getAccount(final TestRestTemplate restTemplate, final String id) {
        return restTemplate.getForEntity("/account/qry/id/" + id, AccountDto.class);
    }

    public static ResponseEntity<AccountDto> getAccountByName(final TestRestTemplate restTemplate, final String name) {
        return restTemplate.getForEntity("/account/qry/name/" + name, AccountDto.class);
    }

    public static ResponseEntity<List<AccountDto>> getAllAccounts(final TestRestTemplate restTemplate) {
        return restTemplate.exchange("/account/qry/all", HttpMethod.GET, null, new ParameterizedTypeReference<>() {});
    }

    public static JournalDto createJournalOk(final TestRestTemplate restTemplate, final CreateJournalDto dto) {
        final var response = restTemplate.postForEntity("/journal/cmd/create", dto, JournalDto.class);
        assertNotNull(response);
        assertNotNull(response.getBody());
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        return response.getBody();
    }

    public static ResponseEntity<JournalDto> getJournal(final TestRestTemplate restTemplate, final String id) {
        return restTemplate.getForEntity("/journal/qry/id/" + id, JournalDto.class);
    }

    public static ResponseEntity<List<JournalDto>> getAllJournals(TestRestTemplate restTemplate) {
        return restTemplate.exchange("/journal/qry/all", HttpMethod.GET, null, new ParameterizedTypeReference<>() {});
    }

    public static ResponseEntity<LedgerDto> getLedger(TestRestTemplate restTemplate, String id) {
        return restTemplate.getForEntity("/ledger/qry/id/" + id, LedgerDto.class);
    }

    public static List<LedgerDto> getAllLedgersForAccount(final TestRestTemplate restTemplate, final String accountId) {
        final var response = restTemplate.exchange("/ledger/qry/account/" + accountId, HttpMethod.GET, null, new ParameterizedTypeReference<List<LedgerDto>>() {});
        assertNotNull(response);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        return response.getBody();
    }

    public static ResponseEntity<String> getString(final String url, final TestRestTemplate restTemplate) {
        return restTemplate.getForEntity(url, String.class);
    }
}
