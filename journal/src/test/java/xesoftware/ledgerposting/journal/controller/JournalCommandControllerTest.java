package xesoftware.ledgerposting.journal.controller;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import xesoftware.ledgerposting.common.error.ErrorResponse;
import xesoftware.ledgerposting.journal.dto.CreateJournalDto;
import xesoftware.ledgerposting.journal.dto.JournalDto;
import xesoftware.ledgerposting.journal.entity.JournalAccount;
import xesoftware.ledgerposting.journal.repository.AccountRepository;

import java.sql.Timestamp;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(
        webEnvironment=SpringBootTest.WebEnvironment.RANDOM_PORT,
        properties = {"app.kafka.enabled=false" })
@ActiveProfiles("command")
class JournalCommandControllerTest {

    @MockBean
    private AccountRepository accountRepository;

    @BeforeEach
    public void beforeEach() {
        Mockito.when(accountRepository.findById("_ac1")).thenReturn(Optional.of(new JournalAccount("_ac1", "", true)));
        Mockito.when(accountRepository.findById("XXX")).thenReturn(Optional.empty());
    }

    @Autowired
    TestRestTemplate restTemplate;

    @Test
    public void testCreateJournal() {
        final CreateJournalDto createJournalDto = new CreateJournalDto(Timestamp.from(Instant.now().truncatedTo(ChronoUnit.SECONDS)).toString(), "123.45", "_ac1");
        final ResponseEntity<JournalDto> response = createJournal(restTemplate, createJournalDto);
        assertNotNull(response);
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        final JournalDto journalDto = response.getBody();
        assertNotNull(journalDto);
        assertEquals(createJournalDto.getAccountId(), journalDto.getAccountId());
        assertEquals(createJournalDto.getTimestamp(), journalDto.getTimestamp());
        assertEquals(createJournalDto.getAmount(), journalDto.getAmount());
    }

    @Test
    public void testCreateJournal_Fail1() {
        ErrorResponse errorResponse = checkBadJournal( new CreateJournalDto(Timestamp.from(Instant.now().truncatedTo(ChronoUnit.SECONDS)).toString(), "123.45", "XXX"));
        assertEquals("Could not create journal entry as account provided with id 'XXX' could not be found", errorResponse.description());
    }

    @Test
    public void testCreateJournal_Fail2() {
        ErrorResponse errorResponse = checkBadJournal(new CreateJournalDto("Not Valid Timestamp", "123.45", "XXX"));
        assertEquals("Timestamp must be in a valid format (yyyy-mm-dd or yyyy-mm-dd hh:mm:ss)", errorResponse.description());
    }

    @Test
    public void testCreateJournal_Fail3() {
        ErrorResponse errorResponse = checkBadJournal(new CreateJournalDto(null, "123.45", "XXX"));
        assertEquals("Timestamp must not be empty", errorResponse.description());
    }

    @Test
    public void testCreateJournal_Fail4() {
        ErrorResponse errorResponse = checkBadJournal(new CreateJournalDto(Timestamp.from(Instant.now().truncatedTo(ChronoUnit.SECONDS)).toString(), "xxx.xx", "XXX"));
        assertEquals("Amount must be a number", errorResponse.description());
    }

    private ErrorResponse checkBadJournal(final CreateJournalDto createJournalDto) {
        final ResponseEntity<ErrorResponse> response = createBadJournal(restTemplate, createJournalDto);
        assertNotNull(response);
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        final ErrorResponse errorResponse = response.getBody();
        assertNotNull(errorResponse);
        assertEquals(400, errorResponse.status());
        assertEquals("400 BAD_REQUEST", errorResponse.error());
        assertEquals("/journal/cmd/create", errorResponse.path());
        return errorResponse;
    }

    public static ResponseEntity<JournalDto> createJournal(TestRestTemplate restTemplate, CreateJournalDto createJournalDto) {
        return restTemplate.postForEntity("/journal/cmd/create", createJournalDto, JournalDto.class);
    }

    public static ResponseEntity<ErrorResponse> createBadJournal(TestRestTemplate restTemplate, CreateJournalDto createJournalDto) {
        return restTemplate.postForEntity("/journal/cmd/create", createJournalDto, ErrorResponse.class);
    }
}