package xesoftware.ledgerposting.journal.controller;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import xesoftware.ledgerposting.common.error.ErrorResponse;
import xesoftware.ledgerposting.journal.dto.JournalDto;
import xesoftware.ledgerposting.journal.entity.Journal;
import xesoftware.ledgerposting.journal.repository.JournalRepository;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(
        webEnvironment=SpringBootTest.WebEnvironment.RANDOM_PORT,
        properties = {"app.kafka.enabled=false" })
@ActiveProfiles("query")
class JournalQueryControllerTest {
    @Autowired
    TestRestTemplate restTemplate;

    @MockBean
    private JournalRepository journalRepository;

    @BeforeEach
    public void beforeEach() {
        Mockito.when(journalRepository.findById("999")).thenReturn(Optional.of(new Journal(new JournalDto("999", "9999-01-01 00:00:00.0", "123", "XYZ"))));
        Mockito.when(journalRepository.findById("XXX")).thenReturn(Optional.empty());
    }

    @Test
    public void testQueryAccount_OK() {
        final ResponseEntity<JournalDto> response = getJournal(restTemplate, "999");
        assertNotNull(response);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals("999", response.getBody().getId());
        assertEquals("XYZ", response.getBody().getAccountId());
        assertEquals("9999-01-01 00:00:00.0", response.getBody().getTimestamp());
    }

    @Test
    public void testQueryAccount_Fail() {
        final ErrorResponse response = checkBadJournal("XXX");
        assertEquals("Journal with id 'XXX', was not found", response.description());
    }

    @Test
    public void testQueryAccounts() {
        final ResponseEntity<List<JournalDto>> response = getAllJournals(restTemplate);
        assertNotNull(response);
        assertNotNull(response.getBody());
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(0, response.getBody().size());
    }

    private ErrorResponse checkBadJournal(final String id) {
        final ResponseEntity<ErrorResponse> response = getBadJournal(restTemplate, id);
        assertNotNull(response);
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        final ErrorResponse errorResponse = response.getBody();
        assertNotNull(errorResponse);
        assertEquals(404, errorResponse.status());
        assertEquals("404 NOT_FOUND", errorResponse.error());
        assertEquals("/journal/qry/id/" + id, errorResponse.path());
        return errorResponse;
    }

    public static ResponseEntity<JournalDto> getJournal(TestRestTemplate restTemplate, String id) {
        return restTemplate.getForEntity("/journal/qry/id/" + id, JournalDto.class);
    }

    public static ResponseEntity<ErrorResponse> getBadJournal(TestRestTemplate restTemplate, String id) {
        return restTemplate.getForEntity("/journal/qry/id/" + id, ErrorResponse.class);
    }

    public static ResponseEntity<List<JournalDto>> getAllJournals(TestRestTemplate restTemplate) {
        return restTemplate.exchange("/journal/qry/all", HttpMethod.GET, null, new ParameterizedTypeReference<List<JournalDto>>() {});
    }
}