package xesoftware.ledgerposting.journal.service;

import xesoftware.ledgerposting.journal.dto.JournalDto;

import java.util.List;
import java.util.Optional;

public interface JournalQueryService {
    Optional<JournalDto> get(String id);
    List<JournalDto> getAll();
}
