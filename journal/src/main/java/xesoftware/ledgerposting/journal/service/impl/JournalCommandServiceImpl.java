package xesoftware.ledgerposting.journal.service.impl;

import lombok.NonNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import xesoftware.ledgerposting.common.error.InvalidRequestException;
import xesoftware.ledgerposting.journal.dto.CreateJournalDto;
import xesoftware.ledgerposting.journal.dto.JournalDto;
import xesoftware.ledgerposting.journal.entity.JournalAccount;
import xesoftware.ledgerposting.journal.kafka.JournalEvent;
import xesoftware.ledgerposting.journal.kafka.KafkaInfo;
import xesoftware.ledgerposting.journal.repository.AccountRepository;
import xesoftware.ledgerposting.journal.service.JournalCommandService;

import javax.security.auth.login.AccountNotFoundException;
import java.util.Optional;
import java.util.UUID;

@Service
public class JournalCommandServiceImpl implements JournalCommandService {
    @Value("${app.kafka.enabled:false}")
    private boolean kafkaEnabled;
    private final KafkaTemplate<String, JournalEvent> kafkaTemplate;
    final AccountRepository accountRepository;

    public JournalCommandServiceImpl(@NonNull final KafkaTemplate<String, JournalEvent> kafkaTemplate,
                                     @NonNull final AccountRepository accountRepository) {
        this.kafkaTemplate = kafkaTemplate;
        this.accountRepository = accountRepository;
    }

    @Override
    public JournalDto create(@NonNull final CreateJournalDto createDto) throws AccountNotFoundException {
        final Optional<JournalAccount> accountOptional = this.accountRepository.findById(createDto.getAccountId());
        return accountOptional.map(account -> {
            final JournalDto dto = new JournalDto();
            dto.setId(UUID.randomUUID().toString().replace("-", ""));
            dto.setAccountId(createDto.getAccountId());
            dto.setTimestamp(createDto.getTimestamp());
            dto.setAmount(createDto.getAmount());

            System.out.println("Kafka enabled = " + kafkaEnabled);

            // TODO: check kafka broker is running.
            // https://stackoverflow.com/questions/37920923/how-to-check-whether-kafka-server-is-running
            if (kafkaEnabled) {
                kafkaTemplate.send(KafkaInfo.TOPIC, new JournalEvent(KafkaInfo.CREATED_EVENT, dto, account.getName(), account.isDebitAccount()));
            }

            return dto;
        }).orElseThrow(
            () -> new AccountNotFoundException("Could not create journal entry as account provided with id '" + createDto.getAccountId() + "' could not be found")
        );
    }
}
