package xesoftware.ledgerposting.journal.service.impl;

import org.springframework.stereotype.Service;
import xesoftware.ledgerposting.journal.dto.JournalDto;
import xesoftware.ledgerposting.journal.repository.JournalRepository;
import xesoftware.ledgerposting.journal.service.JournalQueryService;

import java.util.List;
import java.util.Optional;

@Service
public class JournalQueryServiceImpl implements JournalQueryService {
    final JournalRepository journalRepository;

    public JournalQueryServiceImpl(final JournalRepository journalRepository) {
        this.journalRepository = journalRepository;
    }

    @Override
    public Optional<JournalDto> get(final String id) {
        return this.journalRepository.findById(id).map(JournalDto::new);
    }

    @Override
    public List<JournalDto> getAll() {
        return this.journalRepository.findAll().stream().map(JournalDto::new).toList();
    }
}
