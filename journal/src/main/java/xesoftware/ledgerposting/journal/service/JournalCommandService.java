package xesoftware.ledgerposting.journal.service;

import lombok.NonNull;
import xesoftware.ledgerposting.journal.dto.CreateJournalDto;
import xesoftware.ledgerposting.journal.dto.JournalDto;

public interface JournalCommandService {
    class AccountNotFoundException extends Exception {
        public AccountNotFoundException(String message) {super(message);}
    }
    JournalDto create(@NonNull final CreateJournalDto dto) throws AccountNotFoundException;
}
