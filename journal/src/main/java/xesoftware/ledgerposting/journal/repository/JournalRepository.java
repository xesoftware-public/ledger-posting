package xesoftware.ledgerposting.journal.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import xesoftware.ledgerposting.journal.entity.Journal;

public interface JournalRepository extends JpaRepository<Journal, String> {
}
