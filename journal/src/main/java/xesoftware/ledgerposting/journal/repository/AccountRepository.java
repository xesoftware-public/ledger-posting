package xesoftware.ledgerposting.journal.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import xesoftware.ledgerposting.journal.entity.JournalAccount;

public interface AccountRepository extends JpaRepository<JournalAccount, String> {
}
