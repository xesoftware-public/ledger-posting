package xesoftware.ledgerposting.journal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import xesoftware.ledgerposting.common.error.GlobalExceptionHandler;

@SpringBootApplication
public class JournalApplication {

	public static void main(String[] args) {
		SpringApplication.run(JournalApplication.class, args);
	}

	@Primary
	@Bean
	public GlobalExceptionHandler createGlobalExceptionHandler() {
		// required so that springboot can find this bean from the common library.
		// https://stackoverflow.com/questions/18347141/spring-3-2-controlleradvice-not-working
		return new GlobalExceptionHandler();
	}
}
