package xesoftware.ledgerposting.journal.controller;

import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import xesoftware.ledgerposting.common.error.InvalidRequestException;
import xesoftware.ledgerposting.journal.dto.CreateJournalDto;
import xesoftware.ledgerposting.journal.dto.JournalDto;
import xesoftware.ledgerposting.journal.service.JournalCommandService;

@RestController
@RequestMapping("journal/cmd")
@Profile("command")
public class JournalCommandController {

    final JournalCommandService journalCommandService;

    public JournalCommandController(final JournalCommandService journalCommandService) {
        this.journalCommandService = journalCommandService;
    }

    @PostMapping("/create")
    @ResponseStatus( HttpStatus.CREATED )
    public JournalDto createAccount(@RequestBody final CreateJournalDto dto){
        dto.getErrorMessageIfInvalid().ifPresent(errMsg -> {throw new InvalidRequestException(errMsg);});
        try {
            return this.journalCommandService.create(dto);
        } catch (JournalCommandService.AccountNotFoundException e) {
            throw new InvalidRequestException(e.getMessage());
        }
    }

    @GetMapping("/ok")
    public String ok() {
        return "OK";
    }
}
