package xesoftware.ledgerposting.journal.controller;

import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import xesoftware.ledgerposting.common.error.InvalidRequestException;
import xesoftware.ledgerposting.journal.dto.JournalDto;
import xesoftware.ledgerposting.journal.service.JournalQueryService;

import java.util.List;

@RestController
@RequestMapping("journal/qry")
@Profile("query")
public class JournalQueryController {
    final JournalQueryService journalQueryService;

    public JournalQueryController(final JournalQueryService journalQueryService) {
        this.journalQueryService = journalQueryService;
    }

    @GetMapping("/id/{id}")
    @ResponseStatus( HttpStatus.OK )
    public JournalDto get(@PathVariable(name = "id") final String id){
        return this.journalQueryService.get(id)
                .orElseThrow(() -> new InvalidRequestException(HttpStatus.NOT_FOUND, "Journal with id '" + id + "', was not found"));
    }

    @GetMapping("/all")
    @ResponseStatus( HttpStatus.OK )
    public List<JournalDto> getAll(){
        return this.journalQueryService.getAll();
    }

    @GetMapping("/ok")
    public String ok() {
        return "OK";
    }
}
