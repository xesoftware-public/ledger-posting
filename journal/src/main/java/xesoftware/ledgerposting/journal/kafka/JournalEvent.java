package xesoftware.ledgerposting.journal.kafka;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import xesoftware.ledgerposting.journal.dto.JournalDto;

@Data
@NoArgsConstructor
public class JournalEvent {
    private String type;
    private JournalDto dto;
    private String accountName;
    private boolean debitAccount;

    public JournalEvent(@NonNull final String type, @NonNull final JournalDto dto, @NonNull final String accountName, final boolean debitAccount) {
        this.type = type;
        this.dto = dto;
        this.accountName = accountName;
        this.debitAccount = debitAccount;
    }
}
