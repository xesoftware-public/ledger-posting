package xesoftware.ledgerposting.journal.kafka;

import lombok.NonNull;
import org.springframework.context.annotation.Profile;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;
import xesoftware.ledgerposting.journal.repository.AccountRepository;

/**
 * AccountKafkaListener listens to account created events (from the write enabled instance of the account service) and updates the in-memory database for this
 * read enabled instance of the account service.
 */
@Service
@Profile("query") // Both query and command need to listen for account changes.
public class AccountKafkaListenerForJournalQuery extends AbstractAccountKafkaListener {

    public AccountKafkaListenerForJournalQuery(@NonNull final AccountRepository accountRepository) {
        super(accountRepository);
    }

    @KafkaListener(topics = KafkaInfo.ACCOUNT_TOPIC, groupId = KafkaInfo.ACCOUNT_GROUP_FOR_QUERY)
    public void processEvent(@NonNull final String event) {
        super.processEvent(event);
    }
}