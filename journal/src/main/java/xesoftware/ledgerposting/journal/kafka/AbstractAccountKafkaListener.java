package xesoftware.ledgerposting.journal.kafka;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.listener.AbstractConsumerSeekAware;
import xesoftware.ledgerposting.journal.entity.JournalAccount;
import xesoftware.ledgerposting.journal.repository.AccountRepository;

import java.util.Map;

/**
 * AccountKafkaListener listens to account created events (from the write enabled instance of the account service) and updates the in-memory database for this
 * read enabled instance of the account service.
 */
@Slf4j
public abstract class AbstractAccountKafkaListener extends AbstractConsumerSeekAware {
    private final AccountRepository accountRepository;
    private final ObjectMapper objectMapper = new ObjectMapper();

    protected AbstractAccountKafkaListener(@NonNull final AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
        log.info("Created AccountKafkaListener");
    }

    @Override
    public void onPartitionsAssigned(Map<org.apache.kafka.common.TopicPartition, Long> assignments, ConsumerSeekCallback callback) {
        super.onPartitionsAssigned(assignments, callback);
        seekToBeginning(); // We always want to start at the beginning of the queue. as no persistent storage.
    }

    protected void processEvent(@NonNull final String event) {

        log.info("Getting Account event for journal {}", event);

        try {
            final AccountEvent accountEvent = objectMapper.readValue(event, AccountEvent.class);

            switch (accountEvent.getType()) {
                case KafkaInfo.CREATED_EVENT:
                    saveAccount(accountEvent);
                    break;
                default:
                    break;
            }

        } catch (final Exception e) {
            log.error("Failed to process Account event", e);
        }
    }

    void saveAccount(@NonNull final AccountEvent accountEvent) {
        final JournalAccount account = new JournalAccount(accountEvent.getDto());
        this.accountRepository.save(account);
        log.info("Saved account " + account.getId() + " in the journals account database.");
    }
}