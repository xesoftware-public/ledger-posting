package xesoftware.ledgerposting.journal.kafka;

public interface KafkaInfo {
    String TOPIC = "journal";
    String GROUP = "journal_group";
    String CREATED_EVENT = "created";
    String ACCOUNT_TOPIC = "account";
    String ACCOUNT_GROUP_FOR_QUERY = "account_group_for_journal_query";
    String ACCOUNT_GROUP_FOR_COMMAND = "account_group_for_journal_command";
}
