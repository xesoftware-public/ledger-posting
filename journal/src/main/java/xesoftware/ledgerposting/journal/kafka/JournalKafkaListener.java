package xesoftware.ledgerposting.journal.kafka;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.listener.AbstractConsumerSeekAware;
import org.springframework.stereotype.Service;
import xesoftware.ledgerposting.journal.entity.Journal;
import xesoftware.ledgerposting.journal.repository.JournalRepository;

import java.util.Map;

/**
 * AccountKafkaListener listens to account created events (from the write enabled instance of the account service) and updates the in-memory database for this
 * read enabled instance of the account service.
 */
@Service
@Slf4j
@Profile("query")
public class JournalKafkaListener extends AbstractConsumerSeekAware {
    private final JournalRepository journalRepository;
    private final ObjectMapper objectMapper = new ObjectMapper();

    public JournalKafkaListener(@NonNull final JournalRepository journalRepository) {
        this.journalRepository = journalRepository;
    }

    @Override
    public void onPartitionsAssigned(Map<org.apache.kafka.common.TopicPartition, Long> assignments, ConsumerSeekCallback callback) {
        super.onPartitionsAssigned(assignments, callback);
        seekToBeginning(); // We always want to start at the beginning of the queue. as no persistent storage.
    }

    @KafkaListener(topics = KafkaInfo.TOPIC, groupId = KafkaInfo.GROUP)
    public void processEvent(@NonNull final String event) {

        log.info("Getting JournalEvent {}", event);

        try {
            final JournalEvent journalEvent = objectMapper.readValue(event, JournalEvent.class);

            switch (journalEvent.getType()) {
                case KafkaInfo.CREATED_EVENT:
                    save(journalEvent);
                    break;
                default:
                    break;
            }

        } catch (final Exception e) {
            log.error("Failed to process JournalEvent", e);
        }
    }

    void save(@NonNull final JournalEvent journalEvent) {
        this.journalRepository.save(new Journal(journalEvent.getDto()));
    }
}