package xesoftware.ledgerposting.journal.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import xesoftware.ledgerposting.journal.entity.Journal;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class JournalDto {
    private String id;
    // @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private String timestamp;
    private String amount;
    private String accountId;

    public JournalDto(@NonNull final Journal journal) {
        this.id = journal.getId();
        this.timestamp = journal.getTimestamp().toString();
        this.amount = journal.getAmount().toString();
        this.accountId = journal.getAccountId();
    }
}
