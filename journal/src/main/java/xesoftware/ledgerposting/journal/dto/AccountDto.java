package xesoftware.ledgerposting.journal.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class AccountDto {
    private String id;
    private String name;
    private String description;
    private boolean debitAccount;
}
