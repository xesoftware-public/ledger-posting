package xesoftware.ledgerposting.journal.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.math.NumberUtils;
import xesoftware.ledgerposting.common.TimestampUtils;

import java.util.Optional;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreateJournalDto {
    private String timestamp;
    private String amount;
    private String accountId;

    public Optional<String> getErrorMessageIfInvalid() {
        return Optional.ofNullable(
                (null == timestamp || timestamp.isEmpty())
                ? "Timestamp must not be empty"
                : (null == TimestampUtils.toTimestamp(timestamp))
                ? TimestampUtils.TIMESTAMP_FORMAT_ERROR_MSG
                : (null == amount || amount.isEmpty())
                ? "Amount must not be empty"
                : (!NumberUtils.isParsable(amount)
                ? "Amount must be a number"
                : (null == accountId || accountId.isEmpty())
                ? "AccountId must not be empty"
                : null));
    }
}
