package xesoftware.ledgerposting.journal.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import xesoftware.ledgerposting.journal.dto.AccountDto;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class JournalAccount {
    @Id
    private String id;
    private String name;
    private boolean debitAccount;

    public JournalAccount(@NonNull final AccountDto dto) {
        this.id = dto.getId();
        this.name = dto.getName();
        this.debitAccount = dto.isDebitAccount();
    }
}
