package xesoftware.ledgerposting.journal.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import xesoftware.ledgerposting.journal.dto.JournalDto;

import java.math.BigDecimal;
import java.sql.Timestamp;

@Entity
@Data
@NoArgsConstructor
public class Journal {
    @Id
    private String id;
    @Column(name="tstamp")
    private Timestamp timestamp;
    private BigDecimal amount;
    private String accountId;

    public Journal(@NonNull final JournalDto dto) {
        this.id = dto.getId();
        this.timestamp = Timestamp.valueOf(dto.getTimestamp());
        this.amount = new BigDecimal(dto.getAmount());
        this.accountId = dto.getAccountId();
    }
}
