package xesoftware.ledgerposting.account.controller;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import xesoftware.ledgerposting.account.dto.AccountDto;
import xesoftware.ledgerposting.account.dto.CreateAccountDto;
import xesoftware.ledgerposting.account.entity.Account;
import xesoftware.ledgerposting.account.repository.AccountRepository;
import xesoftware.ledgerposting.account.service.AccountQueryService;
import xesoftware.ledgerposting.common.error.ErrorResponse;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(
        webEnvironment=SpringBootTest.WebEnvironment.RANDOM_PORT,
        properties = {"app.kafka.enabled=false" })
@ActiveProfiles("query")
class AccountQueryControllerTest {
    @Autowired
    TestRestTemplate restTemplate;

    @MockBean
    private AccountRepository accountRepository;

    @BeforeEach
    public void beforeEach() {
        Mockito.when(accountRepository.findById("999")).thenReturn(Optional.of(new Account(new AccountDto("999", "name", "description", true))));
        Mockito.when(accountRepository.findById("NotFound")).thenReturn(Optional.empty());
    }

    @Test
    public void testQueryAccount() {
        final ResponseEntity<AccountDto> response = getAccount(restTemplate, "999");
        assertNotNull(response);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        final AccountDto accountDto = response.getBody();
        assertNotNull(accountDto);
        assertEquals(true, accountDto.isDebitAccount());
        assertEquals("999", accountDto.getId());
        assertEquals("name", accountDto.getName());
        assertEquals("description", accountDto.getDescription());
    }

    @Test
    public void testQueryAccount_Fail() {
        final ResponseEntity<ErrorResponse> response = getBadAccount(restTemplate, "NotFound");
        assertNotNull(response);
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        final ErrorResponse errorResponse = response.getBody();
        assertNotNull(errorResponse);
        // System.out.println(errorResponse);
        assertEquals(404, errorResponse.status());
        assertEquals("404 NOT_FOUND", errorResponse.error());
        assertEquals("Account with id 'NotFound', was not found", errorResponse.description());
        assertEquals("/account/qry/id/NotFound", errorResponse.path());
    }

    @Test
    public void testQueryAccounts() {
        final ResponseEntity<List<AccountDto>> response = getAllAccounts(restTemplate);
        assertNotNull(response);
        assertNotNull(response.getBody());
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(0, response.getBody().size());
    }

    public static ResponseEntity<AccountDto> getAccount(TestRestTemplate restTemplate, String id) {
        return restTemplate.getForEntity("/account/qry/id/" + id, AccountDto.class);
    }

    public static ResponseEntity<ErrorResponse> getBadAccount(TestRestTemplate restTemplate, String id) {
        return restTemplate.getForEntity("/account/qry/id/" + id, ErrorResponse.class);
    }

    public static ResponseEntity<List<AccountDto>> getAllAccounts(TestRestTemplate restTemplate) {
        return restTemplate.exchange("/account/qry/all", HttpMethod.GET, null, new ParameterizedTypeReference<List<AccountDto>>() {});
    }
}