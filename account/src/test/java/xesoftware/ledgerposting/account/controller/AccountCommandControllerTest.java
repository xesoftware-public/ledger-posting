package xesoftware.ledgerposting.account.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import xesoftware.ledgerposting.account.dto.AccountDto;
import xesoftware.ledgerposting.account.dto.CreateAccountDto;
import xesoftware.ledgerposting.common.error.ErrorResponse;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(
        webEnvironment=SpringBootTest.WebEnvironment.RANDOM_PORT,
        properties = {"app.kafka.enabled=false" })
@ActiveProfiles("command")
class AccountCommandControllerTest {
    @Autowired
    TestRestTemplate restTemplate;

    @Test
    public void testCreateAccounts() {
        final CreateAccountDto createAccountDto = new CreateAccountDto("Asset", "An Asset Account - Debit Account", true);
        final ResponseEntity<AccountDto> response = createAccount(restTemplate, createAccountDto);
        assertNotNull(response);
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        final AccountDto accountDto = response.getBody();
        assertNotNull(accountDto);
        assertEquals(createAccountDto.getName(), accountDto.getName());
        assertEquals(createAccountDto.getDescription(), accountDto.getDescription());
        assertEquals(createAccountDto.isDebitAccount(), accountDto.isDebitAccount());
        assertNotNull(accountDto.getId());
    }

    @Test
    public void testCreateAccounts_Fail_1() {
        final CreateAccountDto createAccountDto = new CreateAccountDto(null, null, true);
        final ResponseEntity<ErrorResponse> response = createBadAccount(restTemplate, createAccountDto);
        assertNotNull(response);
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        final ErrorResponse errorResponse = response.getBody();
        assertNotNull(errorResponse);
        System.out.println(errorResponse);
        assertEquals(400, errorResponse.status());
        assertEquals("400 BAD_REQUEST", errorResponse.error());
        assertEquals("Name must not be empty", errorResponse.description());
        assertEquals("/account/cmd/create", errorResponse.path());
    }

    @Test
    public void testCreateAccounts_Fail_2() {
        final CreateAccountDto createAccountDto = new CreateAccountDto("Name", null, true);
        final ResponseEntity<ErrorResponse> response = createBadAccount(restTemplate, createAccountDto);
        assertNotNull(response);
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        final ErrorResponse errorResponse = response.getBody();
        assertNotNull(errorResponse);
        System.out.println(errorResponse);
        assertEquals(400, errorResponse.status());
        assertEquals("400 BAD_REQUEST", errorResponse.error());
        assertEquals("Description must not be empty", errorResponse.description());
        assertEquals("/account/cmd/create", errorResponse.path());
    }

    public static ResponseEntity<AccountDto> createAccount(TestRestTemplate restTemplate, CreateAccountDto createAccountDto) {
        return restTemplate.postForEntity("/account/cmd/create", createAccountDto, AccountDto.class);
    }

    public static ResponseEntity<ErrorResponse> createBadAccount(TestRestTemplate restTemplate, CreateAccountDto createAccountDto) {
        return restTemplate.postForEntity("/account/cmd/create", createAccountDto, ErrorResponse.class);
    }
}