package xesoftware.ledgerposting.account.service;

import xesoftware.ledgerposting.account.dto.AccountDto;

import java.util.List;
import java.util.Optional;

public interface AccountQueryService {
    Optional<AccountDto> get(String id);
    List<AccountDto> getAll();
    Optional<AccountDto> findFirstByName(String name);
}
