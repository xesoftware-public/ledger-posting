package xesoftware.ledgerposting.account.service.impl;

import org.springframework.stereotype.Service;
import xesoftware.ledgerposting.account.dto.AccountDto;
import xesoftware.ledgerposting.account.repository.AccountRepository;
import xesoftware.ledgerposting.account.service.AccountQueryService;

import java.util.List;
import java.util.Optional;

@Service
public class AccountQueryServiceImpl implements AccountQueryService {
    final AccountRepository accountRepository;

    public AccountQueryServiceImpl(final AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @Override
    public Optional<AccountDto> get(final String id) {
        return this.accountRepository.findById(id).map(AccountDto::new);
    }

    @Override
    public List<AccountDto> getAll() {
        return this.accountRepository.findAll().stream().map(AccountDto::new).toList();
    }

    @Override
    public Optional<AccountDto> findFirstByName(String name) {
        return this.accountRepository.findAccountsByName(name).stream().findFirst().map(AccountDto::new);
    }
}
