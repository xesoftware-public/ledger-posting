package xesoftware.ledgerposting.account.service;

import lombok.NonNull;
import xesoftware.ledgerposting.account.dto.AccountDto;
import xesoftware.ledgerposting.account.dto.CreateAccountDto;

public interface AccountCommandService {
    AccountDto create(@NonNull final CreateAccountDto dto);
}
