package xesoftware.ledgerposting.account.service.impl;

import lombok.NonNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import xesoftware.ledgerposting.account.dto.AccountDto;
import xesoftware.ledgerposting.account.dto.CreateAccountDto;
import xesoftware.ledgerposting.account.kafka.AccountEvent;
import xesoftware.ledgerposting.account.kafka.KafkaInfo;
import xesoftware.ledgerposting.account.service.AccountCommandService;

import java.util.UUID;

@Service
public class AccountCommandServiceImpl implements AccountCommandService {

    @Value("${app.kafka.enabled:false}")
    private boolean kafkaEnabled;
    private final KafkaTemplate<String, AccountEvent> kafkaTemplate;

    public AccountCommandServiceImpl(@NonNull final KafkaTemplate<String, AccountEvent> kafkaTemplate) {
        this.kafkaTemplate = kafkaTemplate;
    }

    @Override
    public AccountDto create(@NonNull final CreateAccountDto createDto) {
        final AccountDto ret = new AccountDto();
        ret.setId(UUID.randomUUID().toString().replace("-", ""));
        ret.setName(createDto.getName());
        ret.setDescription(createDto.getDescription());
        ret.setDebitAccount(createDto.isDebitAccount());

        // TODO: check kafka broker is running.
        // https://stackoverflow.com/questions/37920923/how-to-check-whether-kafka-server-is-running
        if (kafkaEnabled) {
            System.out.println("Kafka IS enabled - sending created event");
            kafkaTemplate.send(KafkaInfo.TOPIC, new AccountEvent(KafkaInfo.CREATED_EVENT, ret));
        } else {
            System.out.println("Kafka IS NOT enabled!");
        }

        return ret;
    }
}
