package xesoftware.ledgerposting.account.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import xesoftware.ledgerposting.account.entity.Account;

import java.util.List;

public interface AccountRepository extends JpaRepository<Account, String> {
    List<Account> findAccountsByName(String name);
}
