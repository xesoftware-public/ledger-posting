package xesoftware.ledgerposting.account.kafka;

public interface KafkaInfo {
    String TOPIC = "account";
    String GROUP = "account_group";
    String CREATED_EVENT = "created";
}
