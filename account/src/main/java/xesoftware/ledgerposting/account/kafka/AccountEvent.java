package xesoftware.ledgerposting.account.kafka;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import xesoftware.ledgerposting.account.dto.AccountDto;

@Data
@NoArgsConstructor
public class AccountEvent {
    private String type;
    private AccountDto dto;

    public AccountEvent(@NonNull final String type, @NonNull final AccountDto dto) {
        this.type = type;
        this.dto = dto;
    }
}
