package xesoftware.ledgerposting.account.kafka;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.common.TopicPartition;
import org.springframework.context.annotation.Profile;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.listener.AbstractConsumerSeekAware;
import org.springframework.stereotype.Service;
import xesoftware.ledgerposting.account.entity.Account;
import xesoftware.ledgerposting.account.repository.AccountRepository;

import java.util.Map;

/**
 * AccountKafkaListener listens to account created events (from the write enabled instance of the account service) and updates the in-memory database for this
 * read enabled instance of the account service.
 */
@Service
@Slf4j
@Profile("query")
public class AccountKafkaListener extends AbstractConsumerSeekAware {
    private final AccountRepository accountRepository;
    private final ObjectMapper objectMapper = new ObjectMapper();

    public AccountKafkaListener(@NonNull final AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
        log.info("Created AccountKafkaListener");
    }

    @Override
    public void onPartitionsAssigned(Map<TopicPartition, Long> assignments, ConsumerSeekCallback callback) {
        super.onPartitionsAssigned(assignments, callback);
        seekToBeginning(); // We always want to start at the beginning of the queue. as no persistent storage.
    }

    @KafkaListener(topics = KafkaInfo.TOPIC, groupId = KafkaInfo.GROUP)
    public void processEvent(@NonNull final String event) {

        log.info("Getting Account event {}", event);

        try {
            final AccountEvent accountEvent = objectMapper.readValue(event, AccountEvent.class);

            switch (accountEvent.getType()) {
                case KafkaInfo.CREATED_EVENT:
                    saveAccount(accountEvent);
                    break;
                default:
                    break;
            }

        } catch (final Exception e) {
            log.error("Failed to process event", e);
        }
    }

    void saveAccount(@NonNull final AccountEvent accountEvent) {
        final Account account = new Account(accountEvent.getDto());
        this.accountRepository.save(account);
        log.info("Saved account " + account.getId() + " in the query database.");
    }
}