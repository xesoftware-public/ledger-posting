package xesoftware.ledgerposting.account.controller;

import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import xesoftware.ledgerposting.account.dto.AccountDto;
import xesoftware.ledgerposting.account.service.AccountQueryService;
import xesoftware.ledgerposting.common.error.InvalidRequestException;

import java.util.List;

@RestController
@RequestMapping("account/qry")
@Profile("query")
public class AccountQueryController {
    final AccountQueryService accountQueryService;

    public AccountQueryController(final AccountQueryService accountQueryService) {
        this.accountQueryService = accountQueryService;
    }

    @GetMapping("/id/{id}")
    @ResponseStatus( HttpStatus.OK )
    public AccountDto getAccountById(@PathVariable(name = "id") final String id){
        return this.accountQueryService.get(id)
                .orElseThrow(() -> new InvalidRequestException(HttpStatus.NOT_FOUND, "Account with id '" + id + "', was not found"));
    }

    @GetMapping("/name/{name}")
    @ResponseStatus( HttpStatus.OK )
    public AccountDto getAccountByName(@PathVariable(name = "name") final String name){
        return this.accountQueryService.findFirstByName(name)
                .orElseThrow(() -> new InvalidRequestException(HttpStatus.NOT_FOUND, "Account with name '" + name + "', was not found"));
    }

    @GetMapping("/all")
    @ResponseStatus( HttpStatus.OK )
    public List<AccountDto> getAllAccounts(){
        return this.accountQueryService.getAll();
    }

    @GetMapping("/ok")
    public String ok() {
        return "OK";
    }
}
