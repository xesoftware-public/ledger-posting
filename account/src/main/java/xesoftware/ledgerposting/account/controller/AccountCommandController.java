package xesoftware.ledgerposting.account.controller;

import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import xesoftware.ledgerposting.account.dto.AccountDto;
import xesoftware.ledgerposting.account.dto.CreateAccountDto;
import xesoftware.ledgerposting.account.service.AccountCommandService;
import xesoftware.ledgerposting.common.error.InvalidRequestException;

@RestController
@RequestMapping("account/cmd")
@Profile("command")
public class AccountCommandController {

    final AccountCommandService accountCommandService;

    public AccountCommandController(final AccountCommandService accountCommandService) {
        this.accountCommandService = accountCommandService;
    }

    @PostMapping("/create")
    @ResponseStatus( HttpStatus.CREATED )
    public AccountDto createAccount(final @RequestBody CreateAccountDto dto){
        dto.getErrorMessageIfInvalid().ifPresent(errMsg -> {throw new InvalidRequestException(errMsg);});
        return this.accountCommandService.create(dto);
    }

    @GetMapping("/ok")
    public String ok() {
        return "OK";
    }
}
