package xesoftware.ledgerposting.account.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import xesoftware.ledgerposting.account.dto.AccountDto;

@Entity
@Data
@NoArgsConstructor
public class Account {
    @Id
    private String id;
    private String name;
    private String description;
    // https://en.wikipedia.org/wiki/Debits_and_credits - The basic principle is that the account receiving benefit is debited, while the account giving benefit is credited.
    // debitAccount - true - account receiving benefit. i.e. a debit account.
    // debitAccount - false - account giving benefit. i.e. a credit account.
    private boolean debitAccount;

    public Account(@NonNull final AccountDto dto) {
        this.id = dto.getId();
        this.name = dto.getName();
        this.description = dto.getDescription();
        this.debitAccount = dto.isDebitAccount();
    }
}
