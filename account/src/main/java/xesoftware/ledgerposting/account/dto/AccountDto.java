package xesoftware.ledgerposting.account.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import xesoftware.ledgerposting.account.entity.Account;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AccountDto {
    private String id;
    private String name;
    private String description;
    // https://en.wikipedia.org/wiki/Debits_and_credits - The basic principle is that the account receiving benefit is debited, while the account giving benefit is credited.
    // debitAccount - true - account receiving benefit. i.e. a debit account.
    // debitAccount - false - account giving benefit. i.e. a credit account.
    private boolean debitAccount;

    public AccountDto(@NonNull final Account account) {
        this.id = account.getId();
        this.name = account.getName();
        this.description = account.getDescription();
        this.debitAccount = account.isDebitAccount();
    }
}
