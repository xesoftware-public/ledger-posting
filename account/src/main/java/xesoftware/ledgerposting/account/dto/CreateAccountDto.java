package xesoftware.ledgerposting.account.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Optional;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreateAccountDto {
    private String name;
    private String description;
    // https://en.wikipedia.org/wiki/Debits_and_credits - The basic principle is that the account receiving benefit is debited, while the account giving benefit is credited.
    // debitAccount - true - account receiving benefit. i.e. a debit account.
    // debitAccount - false - account giving benefit. i.e. a credit account.
    private boolean debitAccount;

    public Optional<String> getErrorMessageIfInvalid() {
        return Optional.ofNullable(
                (null == name || name.isEmpty())
                ? "Name must not be empty"
                : (null == description || description.isEmpty())
                ? "Description must not be empty"
                : null);
    }
}
