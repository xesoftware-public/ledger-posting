package xesoftware.ledgerposting.kafka;

import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.test.EmbeddedKafkaBroker;
import org.springframework.kafka.test.EmbeddedKafkaZKBroker;

/**
 * Expose SpringBoots embedded kafka broker.
 */
@SpringBootApplication()
public class KafkaApplication {

	public static void main(String[] args) {
		new SpringApplicationBuilder(KafkaApplication.class)
				.web(WebApplicationType.NONE) // Not a web application
				.run(args);
	}

	@Bean
	EmbeddedKafkaBroker broker() {
		return new EmbeddedKafkaZKBroker(1)
				.kafkaPorts(9092)
				.brokerListProperty("spring.kafka.bootstrap-servers");
	}
}
